# Nunet Demo App

The app is live at http://demo.nunet.io. Current status: [v0.1.1 community-beta](https://gitlab.com/nunet/nunet-demo/-/releases#v0.1.1).

NuNet allows to run SingularityNET services on computing resources that are pooled from independent providers and connect several of them to perform a more complex task or workflow. Hardware device providers that participate in the ecosystem are compensated with NuNet tokens. 

This demo app showcases an orchestration of distributed execution of an AI task on separate hardware components and adequately reimbursing them for used computing resources and time. The AI workflow consists of object recognition and dog breed recognition services offered at [SingularityNET](https://singularitynet.io) marketplace. Specifically, the demo implements the proof-of-concept of:

* Execution of business logic which uses multiple SingularityNET AI services connected in ad-hoc task-dependent network;
* The usage of independent hardware environments for decentralized execution of computation processes in independent hardware environments and physical machines;
* Orchestration of the decentralized execution and data exchanges between computation processes running in independent execution environments;
* Economy of computational processes, including estimation of execution costs based on independent resource pricing of providers of execution environments, usage profiling and orchestration of token payments to hardware providers.
* Telemetry of resource usage in decentralized network;

Each new user of the demo app is provided an initial amount of tokens and can initiate AI task execution by submitting a dog picture and the amount of required tokens to cover computing resources. If a dog and its breed is recognized in the submitted image, a user is awarded an additional amount of tokens (depending on the breed of the dog discovered). A user can execute as many tasks as his/her token balance allows.

<img src="webapp/public/architecture/nunet_demo_architecture-01.svg"  width="500">


The full NuNet version will enable computational economy consisting of any processes, hardware devices and computing frameworks. For more, see:

* [Release notes](https://gitlab.com/nunet/nunet-demo/-/releases#v0.1.1)
* [Submitting bug reports and feature requests](https://gitlab.com/nunet/nunet-demo/wikis/Submitting%20bug%20reports%20and%20feature%20requests)
* [Developer flow description](https://gitlab.com/nunet/nunet-demo/wikis/Development%20flow)
* NuNet webpage at https://nunet.io;
* NuNet introductory [video](https://www.youtube.com/watch?v=ju6dQJdTR3g);
* [NuNet Network Status](https://stats.nunet.io).



