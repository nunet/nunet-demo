import docker
from constants import CONTAINER_NAME

if __name__ == "__main__":
    client = docker.from_env()

    net1 = 0
    i = 0
    for net in client.networks.list():
        if net.name == CONTAINER_NAME:
            net1 = net
            print("network already exist")

    if net1 == 0:
        net1 = client.networks.create(name=CONTAINER_NAME, driver="bridge")
        net1.connect(container=CONTAINER_NAME)
        print("new network is created")
    else:
        net1.connect(container=CONTAINER_NAME)
        print("connected")
