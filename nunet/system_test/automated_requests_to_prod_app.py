'''

You need to adjust the settings as described in the file automated_requests/settings.txt

'''

import system_load_test
import random
import time
import logging



def automated_requests():

    sleep_interval_multiplier = [60,200]
    sleep_interval_multiplier_value = 0

    min_sleep = 10
    max_sleep = 20

    min_requests = 1
    max_requests = 3

    while True:

        try:
            print(system_load_test.nunet_address)
            system_load_test.nunet_address_index = 1
            print('Loading',system_load_test.nunet_address[system_load_test.nunet_address_index])
            system_load_test.user_name_prefix = 'auto'

            print('Loading settings ...')
            try:
                with open('automated_requests/settings.txt','r') as f:
                    lines = f.read().splitlines()

                sleep_line_min = lines[1]
                sleep_line_max = lines[2]
                request_line_min = lines[4]
                request_line_max = lines[5]

                indices = [i for i, a in enumerate(sleep_line_min) if a == ',']
                min_sleep = int(sleep_line_min[indices[0]+1:indices[1]])
                indices = [i for i, a in enumerate(sleep_line_max) if a == ',']
                max_sleep = int(sleep_line_max[indices[0] + 1:indices[1]])
                indices = [i for i, a in enumerate(request_line_min) if a == ',']
                min_requests = int(request_line_min[indices[0] + 1:indices[1]])
                indices = [i for i, a in enumerate(request_line_max) if a == ',']
                max_requests = int(request_line_max[indices[0] + 1:indices[1]])
                print('min_sleep =',min_sleep)
                print('max_sleep =',max_sleep)
                print('min_requests =',min_requests)
                print('max_requests =',max_requests)

            except:
                logging.exception('message')
                print('Error occured, reverting to default settings ...')
                min_sleep = 10
                max_sleep = 20

                min_requests = 1
                max_requests = 3

            num_requests = random.randint(min_requests, max_requests)
            print('Number of requests:',num_requests)
            system_load_test.run_load_test(num_requests)

            sleep_time = random.randint(min_sleep*sleep_interval_multiplier[sleep_interval_multiplier_value],max_sleep*sleep_interval_multiplier[sleep_interval_multiplier_value])
            print('Sleeping for:',str(sleep_time/60),'minutes')
            time.sleep(sleep_time)

        except:
            logging.exception('message')


















if __name__ == '__main__':


    automated_requests()

