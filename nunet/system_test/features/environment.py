import os
import time
import logging

try:
    if os.environ['SYSTEM_TEST_ADDRESS'] == 'true':
        SYSTEM_TEST_NUNET_ADDRESS = "https://demo.nunet.io:3031/login"
        print('Using system test nunet instance ...')
    else:
        SYSTEM_TEST_NUNET_ADDRESS = "http://127.0.0.1:3000/login"
        print('Using local nunet instance ...')
except:
    logging.exception('message')
    SYSTEM_TEST_NUNET_ADDRESS = "http://127.0.0.1:3000/login"
    print('Using local nunet instance ...')

from selenium import webdriver

from pyvirtualdisplay import Display
# display = Display(visible=0, size=(1024,768))
# display = Display(visible=0, size=(1920,1080))
display = Display(visible=0, size=(1920,10000))
display.start()

delay_page_load_timeout = 200
delay_implicit_wait = 200
delay_wait_for_container_deletion = 60

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--no-sandbox') # required when running as root user. otherwise you would get no sandbox errors.

def before_all(context):
    # context.browser = [webdriver.Firefox(),webdriver.Chrome(chrome_options=chrome_options)]
    context.browser = [webdriver.Chrome(chrome_options=chrome_options)]
    # context.browser = [webdriver.Firefox()]
    for browser in context.browser:
        browser.set_page_load_timeout(delay_page_load_timeout)
        browser.implicitly_wait(delay_implicit_wait)
        browser.maximize_window()

    # Do first run using only one browser. Add asserts later if you want

    print('*********** Run first task ***********')

    browser_2 = [webdriver.Chrome(chrome_options=chrome_options)]

    for browser in browser_2:
        browser.set_page_load_timeout(delay_page_load_timeout)
        browser.implicitly_wait(delay_implicit_wait)
        browser.maximize_window()
        browser.get(SYSTEM_TEST_NUNET_ADDRESS)
        elem = browser.find_element_by_name("email")
        elem.send_keys("test@test.com")
        elem = browser.find_element_by_name("password")
        elem.send_keys("test")
        browser.find_element_by_xpath("//*[contains(text(), 'Sign In')]").click()
        browser.find_element_by_xpath("//*[contains(text(), 'Execute Task')]").click()
        browser.find_element_by_xpath("//input").send_keys(os.getcwd() + '/test_data/dog_1.jpeg')
        browser.find_element_by_xpath("//*[contains(text(), 'Call Service')]").click()
        time.sleep(delay_wait_for_container_deletion)
        browser.quit()




def after_all(context):
    for browser in context.browser:
        browser.quit()
