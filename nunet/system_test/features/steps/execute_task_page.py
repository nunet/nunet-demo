from behave import *
from selenium.webdriver.common.keys import Keys
import unittest
tc = unittest.TestCase('__init__')
from allure_commons._allure import attach
from allure_commons.types import AttachmentType
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
import os

from user_login import *

delay = 60 # seconds
delay_wait_for_container_deletion = 60


# Run this from within the directory containing the features folder (the automated_tests folder) behave -f allure_behave.formatter:AllureFormatter -o reports/ ./features
# Then run  allure generate reports/ -o html_reports/ to generate html reports from within the same folder
# Finally open the html page in firefox


@given(u'user loggs in')
def step_impl(context):
    context.execute_steps(u'''
            Given user visits login page
            When puts in login details
            Then sees consumer perspecitve page
         ''')

@given(u'presses execute task')
def step_impl(context):
    for browser in context.browser:
        browser.find_element_by_xpath("//*[contains(text(), 'Execute Task')]").click()

@when(u'uploads a valid dog image')
def step_impl(context):

    for browser in context.browser:
        browser.find_element_by_xpath("//input").send_keys(os.getcwd()+'/test_data/dog_1.jpeg')


@then(u'presses call service')
def step_impl(context):
    assert_list = ['Docker Logs', 'Create yolov3 object detection ...', 'Create cntk-image-recon ...',
                   'Calling Object Detection grpc service ...', 'Successfully called Object Detection grpc service ...',
                   'Calling CNTK Image Recognition grpc service ...',
                   'Successfully called CNTK Image Recognition grpc service', 'Containers deleted',
                   'Dog breed is Anatolian_shepherd_dog with', '% confidence level.', 'overall result']

    for browser in context.browser:
        browser.find_element_by_xpath("//*[contains(text(), 'Call Service')]").click()
        for l in assert_list:
            print('***************************')
            print(l)
            WebDriverWait(browser, delay).until(EC.presence_of_all_elements_located((By.XPATH, "//*[contains(text(), '{}')]".format(l))))
            tc.assertIn(l, browser.page_source)

    time.sleep(delay_wait_for_container_deletion)




