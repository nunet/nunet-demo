# don't use [[ testing, since [] is compatible with older and posix shells
if [ "$NUNET_CONTAINER_NAME" = "test_nunet" ]; then
    wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb &&
        dpkg -i google-chrome-stable_current_amd64.deb
    apt-get -fy --fix-missing install &&
        apt -y install firefox &&
        wget https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-linux32.tar.gz &&
        tar -xzf geckodriver-v0.26.0-linux32.tar.gz &&
        mv geckodriver /usr/local/bin/ &&
        wget https://chromedriver.storage.googleapis.com/78.0.3904.70/chromedriver_linux64.zip &&
        apt -y install unzip vim &&
        unzip chromedriver_linux64.zip &&
        mv chromedriver /usr/local/bin/ &&
        cp keyboard /etc/default/ &&
        export DEBIAN_FRONTEND="noninteractive" &&
        apt update &&
        apt install -y openjdk-8-jdk &&
        wget http://repo.maven.apache.org/maven2/io/qameta/allure/allure-commandline/2.13.0/allure-commandline-2.13.0.zip &&
        unzip allure-commandline-2.13.0.zip &&
        apt -y install xvfb &&
        apt install -y xorg xvfb dbus-x11 xfonts-100dpi xfonts-75dpi xfonts-cyrillic &&
        yes | pip install pyvirtualdisplay behave selenium allure-python-commons allure-behave &&
        apt install openssh-client -y &&
        apt install rsync -y &&
        mkdir reports &&
        mkdir html_reports
fi
