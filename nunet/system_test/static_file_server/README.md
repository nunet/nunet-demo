# static_file_server

Serve static folders. Each static folder is assumed to be a web based report having an index.html file and files required by this html script.
Navigating to a folder will open this html file.


This service is deployed using supervisord on the nunet demo server. Please see the supervisord configuration file [system_test_static_server.conf](supervisor/system_test_static_server.conf)


## Managing the service with gunicorn

**_NOTE_**: move the `logos` and `css` directories, found inside the `template_dir`, to the `web_dir` directory, the static file directory which is defined in the app.py file.
### How to run
`cd static_file_server`

edit the gunicorn/config.py to suit paths in the deployment machine

start the flask app

`gunicorn -c gunicorn/config.py app:app`

open `localhost:9999` from a web browser.

### How to stop
open another terminal from the `static_file_server` directory

to stop: `kill $(cat gunicorn/log/pid.pid)`

to restart: `kill -HUP $(cat gunicorn/log/pid.pid)`

Look at [config.py](gunicorn/config.py) for details.
