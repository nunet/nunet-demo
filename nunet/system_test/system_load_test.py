'''

Parameters to run the load test

nunet_address_index: used to choose which version of the app to test against. The index refers to the list nunet_address
user_name_prefix: the users used to do the load testing are incremetnally numbered by appending to this prefix
total_number_of_requests: either obtained through an environmental variable or set in the main section of the code in the except portion of the try-catch statement

'''


import os
import time
import datetime
from multiprocessing import Pool
import logging

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

from pyvirtualdisplay import Display

file_name_pre = ''

nunet_address = ["https://demo.nunet.io:3031/login","https://demo.nunet.io/login","https://demo.nunet.io:3030/login","http://127.0.0.1:3000/login"]
nunet_address_index = 0

user_name_prefix = 'testx'



def run_task(x):


    delay = 420  # seconds
    delay_2 = 3 # delay used to simulate manual operation sometimes needed to wait previous process to finish
    delay_page_load_timeout = 200
    delay_implicit_wait = 200
    delay_wait_for_container_deletion = 60

    browser = None
    ret_message = None

    root_dir = 'system_load_test_logs/'+file_name_pre+'/'+str(x)+'/'
    os.mkdir(root_dir)

    try:

        # Display code might need to be put here for true parallelism?

        # display = Display(visible=0, size=(1024,768))
        # display = Display(visible=0, size=(1920,1080))
        display = Display(visible=0, size=(1920, 1500))
        display.start()

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')  # required when running as root user. otherwise you would get no sandbox errors.

        # End of display code

        print('Running system load test for process number:', str(x))
        ret_message = [0, '', x]

        browser = webdriver.Chrome(chrome_options=chrome_options)
        browser.set_page_load_timeout(delay_page_load_timeout)
        browser.implicitly_wait(delay_implicit_wait)

        time.sleep(delay_2)
        browser.save_screenshot(root_dir+"1.before_maximizing_window-"+str(datetime.datetime.now().time())+".PNG")


        browser.maximize_window()

        time.sleep(delay_2)
        browser.save_screenshot(root_dir+"2.maxmimize_window-"+str(datetime.datetime.now().time())+".PNG")


        # Creating new user
        browser.get(nunet_address[nunet_address_index])

        time.sleep(delay_2)
        browser.save_screenshot(root_dir+"3.visit_login_page-"+str(datetime.datetime.now().time())+".PNG")

        browser.find_element_by_xpath("//*[contains(text(), 'Create Account')]").click()
        elem = browser.find_element_by_name("email")
        elem.send_keys(user_name_prefix+str(x)+"@test.com")
        elem = browser.find_element_by_name("password")
        elem.send_keys("test")
        elem = browser.find_element_by_id("confrimPassword")
        elem.send_keys("test")
        elem = browser.find_element_by_name("agree")

        time.sleep(delay_2)
        browser.save_screenshot(root_dir + "4.before_clicking_signup-"+str(datetime.datetime.now().time())+".PNG")

        elem.click()

        time.sleep(delay_2)
        browser.save_screenshot(root_dir + "5.after_clicking_signup-"+str(datetime.datetime.now().time())+".PNG")

        browser.find_element_by_xpath("//*[contains(text(), 'Sign Up')]").click()
        time.sleep(delay_2)


        # Trying to login and then run service

        browser.get(nunet_address[nunet_address_index])

        time.sleep(delay_2)
        browser.save_screenshot(root_dir + "6.get_login_page-"+str(datetime.datetime.now().time())+".PNG")

        elem = browser.find_element_by_name("email")
        elem.send_keys(user_name_prefix+str(x)+"@test.com")
        elem = browser.find_element_by_name("password")
        elem.send_keys("test")

        time.sleep(delay_2)
        browser.save_screenshot(root_dir + "7.before_signing_in-"+str(datetime.datetime.now().time())+".PNG")

        browser.find_element_by_xpath("//*[contains(text(), 'Sign In')]").click()
        time.sleep(delay_2)

        time.sleep(delay_2)
        browser.save_screenshot(root_dir + "8.after_sigging_in-"+str(datetime.datetime.now().time())+".PNG")

        browser.find_element_by_xpath("//*[contains(text(), 'Execute Task')]").click()

        time.sleep(delay_2)
        browser.save_screenshot(root_dir + "9.after_pressing_execute_task-"+str(datetime.datetime.now().time())+".PNG")

        browser.find_element_by_xpath("//input").send_keys(os.getcwd() + '/test_data/dog_1.jpeg')

        time.sleep(delay_2)
        browser.save_screenshot(root_dir + "10.after_uploading_dog_pic-"+str(datetime.datetime.now().time())+".PNG")

        browser.find_element_by_xpath("//*[contains(text(), 'Call Service')]").click()

        time.sleep(delay_2)
        browser.save_screenshot(root_dir + "11.after_clicking_call_service-"+str(datetime.datetime.now().time())+".PNG")

        # Checking if process has finished
        assert_list = ['Dog breed is Anatolian_shepherd_dog with']


        for l in assert_list:
            WebDriverWait(browser, delay).until(EC.presence_of_all_elements_located((By.XPATH, "//*[contains(text(), '{}')]".format(l))))
            if l in browser.page_source:
                ret_message[0] = 1
                ret_message [1] = 'OK'

            time.sleep(delay_2)
            browser.save_screenshot(root_dir + "12.1.call_ends_with_success-"+str(datetime.datetime.now().time())+".PNG")

    except Exception as e:

        logging.exception("message")
        print('Exxxxxxxxxxxxxxxxxxxxception for',str(x))
        ret_message[1] = str(e)

        time.sleep(delay_2)
        browser.save_screenshot(root_dir + "12.2.call_fails-"+str(datetime.datetime.now().time())+".PNG")


    time.sleep(delay_wait_for_container_deletion)
    try:
        browser.quit()
    except:
        logging.exception("message")
    return ret_message


def run_load_test(num_requests):

    start_time = time.time()

    global file_name_pre

    file_name_pre = str(datetime.datetime.now())
    os.mkdir('system_load_test_logs/'+file_name_pre)

    indices = [x for x in range(num_requests)]

    with Pool(processes=num_requests) as pool:
        result = pool.map(run_task, indices)


    print('rrrrrrrrrrrrrrrrrrr')
    print(result)
    print('rrrrrrrrrrrrrrrrrrr')

    end_time = time.time()

    sum = 0
    for i in result:
        sum += i[0]

    print(time.strftime("%c"))
    print(str(sum)+'/'+str(num_requests)+' succedded')
    print(str(sum/num_requests*100)+'% of requests succedded')

    print('Testing took ' + str(((end_time - start_time))) + ' seconds.')
    print('Testing took ' + str(((end_time - start_time) / 60)) + ' minutes.')

    with open('system_load_test_logs/'+file_name_pre+'.log','w') as f:
        f.write(time.strftime("%c"))
        f.write('\n')
        f.write(str(sum)+'/'+str(num_requests)+' succedded'+'\n')
        f.write(str(sum/num_requests*100)+'% of requests succedded'+'\n')
        f.write('Testing took ' + str(((end_time - start_time))) + ' seconds.'+'\n')
        f.write('Testing took ' + str(((end_time - start_time) / 60)) + ' minutes.'+'\n')
        for r in result:
            if r[0] == 0:
                f.write('For ' + str(r[2]) + '-$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n')
                f.writelines(r[1])



if __name__ == '__main__':


    total_number_of_requests = None
    try:
        total_number_of_requests = os.environ['SELENIUM_SYSTEM_LOAD_TEST']
    except:
        logging.exception('message')
        total_number_of_requests = 2
    run_load_test(total_number_of_requests)




    pass