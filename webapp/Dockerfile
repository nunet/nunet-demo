FROM node:latest AS builder

COPY . /webapp

# change the working directory
WORKDIR /webapp

# docker-compose will not let you get environment variables on build time or build args as environment variables
# these args are used in npm build as environment variable(alas the weird variable setting code)
ARG SERVER_HOST
ENV SERVER_HOST ${SERVER_HOST}
ARG WEB_PORT
ENV WEB_PORT ${WEB_PORT}
ARG GRPC_PORT
ENV GRPC_PORT ${GRPC_PORT}
ARG FACEBOOK_APPID
ENV FACEBOOK_APPID ${FACEBOOK_APPID}
ARG GOOGLE_CLIENT
ENV GOOGLE_CLIENT ${GOOGLE_CLIENT}

RUN yarn install --silent
RUN yarn run build

# start secure nginx server using new build
FROM nginx:latest
COPY --from=builder /webapp/build /var/www/build

# copy nginx configuration file
ARG nginx_config_file
COPY ./nginx/${nginx_config_file} /etc/nginx/conf.d/default.conf

EXPOSE 80 443

# start nginx with wierd command (where in the world making daemon off will start daemon, maybe in nginx world)
CMD ["nginx", "-g", "daemon off;"]
