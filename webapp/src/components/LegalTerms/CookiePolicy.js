import React from 'react';
import Iframe from 'react-iframe';

export default function CookiePolicy(props) {

    return (
        <Iframe url="https://www.iubenda.com/privacy-policy/67148905/cookie-policy"
        height="1000px"
        width="100%"
        styles={{backgroundColor: 'green'}}
        scrolling="yes"
           />
    )
}
