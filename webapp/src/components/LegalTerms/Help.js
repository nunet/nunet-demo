import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import ListItem from '@material-ui/core/ListItem';
import Box from "@material-ui/core/Box";
import Link from "@material-ui/core/Link";
import Copyright from "../commons/Copyright";

import HelpIcon from "../../icons/IconsNunet10";

const useStyles = makeStyles(theme => ({
  gutterTop: {
      paddingTop: theme.spacing(2)
  },
  subLink: {
    ml: "2"
  },
  innerContainer: {
      padding: 0
  },
  half_divider: {
      width: "40%",
      margin: "0 auto"
  },
  under_nunet_icon: {
      marginTop: theme.spacing(-3)
  },
  nunet_icon: {
      height: "8rem",
      marginTop: "0rem"
  }
}));


export default function Help(props){
  const classes = useStyles();

  return(
    <React.Fragment>
      <HelpIcon className={classes.nunet_icon} />
      <Typography
        variant="h6"
        gutterBottom
        className={classes.under_nunet_icon}
      >
        NuNet Platform Demo Application
      </Typography>
      <Divider></Divider>

      <Box align="justify">
        
        <Typography className={classes.gutterTop}><Link href="#general-description">General Description</Link></Typography>

        <Typography><Link href="#views">Views</Link></Typography>
              <Box ml={4}>
                  <Typography><Link href="#home">Home</Link></Typography>
                  <Typography><Link href="#execute-task">Execute Task</Link></Typography>           
                  <Typography><Link href="#result-page">Results Page</Link></Typography>
                  <Typography><Link href="#reward-table">Reward Table</Link></Typography>
                  <Typography><Link href="#consumer-perspective">Consumer Perspective</Link></Typography>
                  <Typography><Link href="#task-history">Task History</Link></Typography>
                  <Typography><Link href="#provider-perspective">Provider Perspective</Link></Typography>
              </Box>

        <Typography><Link href="#telemetry">Telemetry</Link></Typography>
              <Box ml={4}>
                  <Typography><Link href="#total-cpu">Total CPU</Link></Typography>
                  <Typography><Link href="#ram">RAM</Link></Typography>           
                  <Typography><Link href="#max-ram">MaxRAM</Link></Typography>
                  <Typography><Link href="#time">Time</Link></Typography>
                    {// define id for general-description temporary solution for content hidden by appbar
                    }<span id="general-description"></span>
                  <Typography><Link href="#net">NET</Link></Typography>
                  <Typography><Link href="#net-time">NetTime</Link></Typography>
              </Box> 
    </Box>

          {/* Note-- In this page there is a trick which is used as a temporary solutions.
              which when you click on the link to go to where it founds, since the content start from the very beginning of the page, 
              some contents are hidden by the app bar.
              so to solve this the trick used is defining the id corresponding to that text just in the text found before it
              by manually checking how lines the app bar hides
          */}

      
        <Typography className={classes.gutterTop} align="justify" variant="h6">
            General Description
        </Typography>
     
      <Divider></Divider>

        <Typography variant="body1" className={classes.gutterTop} align="justify">
          The NuNet Demo Application is designed to demonstrate the NuNet platform, a global economy of decentralized computing. 
          The demonstration utilizes a simple use-case of dog breed recognition game, 
          where users submit an image of a dog (via mobile phone camera or from disk) and initiate a pipeline of AI services provided by SingularityNET, 
          which subsequently recognize an object and a dog breed.
        </Typography>

        <Typography variant="body1" align="justify"  className={classes.gutterTop}>
            The demo implements the proof-of-concept of:
        </Typography>

        <Typography variant="body2" align="justify" >
            <ListItem>Execution of business logic which uses multiple SingularityNET AI services connected in ad-hoc task-dependent network;</ListItem>
            <ListItem>The usage of independent hardware environments for decentralized execution of computation processes in independent hardware environments and physical machines;</ListItem>
            <ListItem>Orchestration of the decentralized execution and data exchanges between computation processes running in independent execution environments;</ListItem>
            <ListItem>Telemetry of resource usage in decentralized network (see <Link href="#telemetry">Telemetry</Link>);</ListItem>
            <ListItem>Economy of computational processes, including estimation of execution costs based on independent resource pricing of providers of execution environments, usage profiling and orchestration of token payments to hardware providers.</ListItem>
        </Typography>

              <span id="views"> </span>
        <Typography variant="body1" align="justify"  className={classes.gutterTop}>
           The live NuNet platform activity can be observed via Network Statistics page at <Link href="https://stats.nunet.io" target="_blank">https://stats.nunet.io</Link>;
        </Typography>
              
            <span id="home"> </span>
      <br />
      <Typography align="justify" variant="h6">Views</Typography>
       <Divider></Divider>

      <Typography className={classes.gutterTop} align="justify" variant="h6">Home</Typography>
      <br />
        <Typography variant="body1" align="justify">
            The home screen shortly introduces the logic of an application and the dog breed recognition game. 
            Home screen is displayed only for when a user logins for the first time, 
            otherwise a user is directed immediately to 'Execute Task' page. 
            Each new user is awarded an initial amount of NTXd tokens for covering the costs of decentralized hardware usage on the platform. 
                  <span id="execute-task"> </span>
            Successful tasks allow a user to earn more tokens and execute more tasks.
      </Typography>

      <br />
      <Typography className={classes.gutterTop} align="justify" variant="h6">Execute Task</Typography>
      <br />
          <Typography variant="body1" align="justify">
              Execute Task view exposes the main functionality of the application by:
          </Typography>
          <Box ml={2}>
            <Typography variant="body2" className={classes.gutterTop} align="justify">
                    1. Allowing users to submit an image of the dog;
                <br />
                    2. Showing the progress, process statistics and telemetry information 
                  (see <Link href="#telemetry">Telemetry</Link>) of the object detection process 
                  (separate <Link href="https://beta.singularitynet.io/servicedetails/org/snet/service/yolov3-object-detection">SingularityNET service</Link> 
                  running in isolated environment);
                <br />
                    3. Showing the progress, process statistics and telemetry information of the dog breed detection process (separate <Link href="https://beta.singularitynet.io/servicedetails/org/snet/service/cntk-image-recon">SingularityNET service</Link> running in isolated environment);
            
            </Typography>
          </Box>

          <Typography variant="body1" className={classes.gutterTop} align="justify">
              The application automatically steps through these stages and presents results of each of them on the screen as the 
              corresponding processes are carried out by decentralized execution environments and hardware devices within the platform. 
              <span id="result-page"> </span>
              After processes finish, a user can view overall results on the separate page;
          </Typography>

          <Typography className={classes.gutterTop} align="justify" variant="h6" >Result Page</Typography>
          <br />
            <Typography variant="body1" align="justify">
              The Results page provides task execution summary by displaying the initial picture with the highlighted object box and the name of detected breed with highest confidence as judged by the algorithm. A user can also check the raw JSON output of the dog breed detection algorithm which provides all candidate dog breeds with their probability estimations. A user is able to share the result screen via his/her social media accounts (Twitter, Telegram, LinkedIn and Facebook).
            </Typography>

            <Typography variant="body1" align="justify" className={classes.gutterTop}>
              The summarized telemetry information of computational resources used by the NuNet Platform for the execution of a a task task is displayed under the title 'Resources'.
            </Typography>

            <Typography variant="body1" align="justify" className={classes.gutterTop}>
                The tokenomic aspect of the executed task is provided under the title 'Tokens'. 
                  <span id="reward-table"> </span> 
                This includes reward received for completing the task, cost of used computing resources and the earnings balance in NTXd tokens.
            </Typography>

            <Typography className={classes.gutterTop} align="justify" variant="h6">Reward Table</Typography>
            <br />
                <span id="consumer-perspective"> </span>
                <Typography variant="body1" align="justify">
                  The Reward Table page displays the list of all detectable dog breeds, their sample images and corresponding bounty rewards in NTXd.
                </Typography>

            <Typography className={classes.gutterTop} align="justify" variant="h6">Consumer Perspective</Typography>
            <br />
                <Typography variant="body1" align="justify">
                    In NuNet, consumers are users of the platform which use it primarily for defining and executing AI tasks. This view summarizes consumer's activity in terms of the number of submitted and successful tasks, balance of NTXd tokens in consumer's account, amounts of NTXd paid to hardware providers and earned from successful tasks. For registered users, the view displays balance of all historic activity, for guest users -- only for tasks and processes executed during a single session.
                </Typography>
            <br />
                <Typography variant="body1" align="justify">
                    On the fully developed NuNet platform, consumers will be able to earn tokens by providing combinations of available AI services and hardware resources to other consumers which are in need of specific business logic. 
                      <span id="task-history"> </span>
                    The present demo application shows a simple case of what will grow into the global economy of decentralized computing processes.
                </Typography>
              
            <Typography className={classes.gutterTop} align="justify" variant="h6">Task History</Typography>
            <br />
                <Typography variant="body1" align="justify">
                   This view allows consumers to access the list of all executed tasks and their result pages. For registered users, the history of activity in the platform is stored in internal database and can be accessed anytime. 
                      <span id="provider-perspective"> </span>
                   Guest users' activity is flushed on logout or browser close and is therefore lost.
                </Typography>
            
               
            <Typography className={classes.gutterTop} align="justify" variant="h6">Provider Perspective</Typography>
            <br />
                <Typography variant="body1" align="justify">
                  The computational capacity of NuNet's platform is composed from computational capacities of decentralized hardware devices, owned and registered to the platform by independent providers. The API of APIs of NuNet will provide a way for hardware providers to register their computational capacities and set their usage price in NTXd and for consumers to bid for those resources when executing computational tasks.
                </Typography>

                <Typography variant="body1" align="justify" className={classes.gutterTop}>
                  Provider perspective view allows for hardware providers to access the full history of the usage of their devices, including computational processes completed on each registered device and NTXd tokens earned by them.
                </Typography>

                <Typography variant="body1" align="justify" className={classes.gutterTop}>
                   Further, providers can inspect each device separately and access:
                </Typography>

                <Typography variant="body2" className={classes.gutterTop} align="justify">
                    <ListItem>limits of computing resources dedicated to the usage on NuNet platform by each device 
                      (in terms of their CPU, RAM, Network time and system time);</ListItem>
                    <ListItem>historic usage of a device in the platform in terms of used computing resources;</ListItem>
                    <ListItem>price of computing resources for each device.</ListItem>
                </Typography>
                
                <span id="telemetry"></span>
                <Typography variant="body1" align="justify">
                Fully developed NuNet platform will allow providers to fully manage their pool of hardware devices: register new devices, set and change their computing resource prices manually or via a learning algorithms, etc.
                </Typography>

                <span id="total-cpu"></span>
      <Typography className={classes.gutterTop} align="justify" variant="h6">Telemetry</Typography>
      <Divider></Divider>
      
        <Typography className={classes.gutterTop} gutterBottom align="justify" variant="h6">Total CPU</Typography>
          <Typography variant="body1" align="justify">
              Aggregated CPU usage of all machines in the network, measured in million CPU ticks. 
              <span id="ram"> </span>
              <b>MTicks</b> (million ticks) metric shows how much CPU time was used during the execution of the measured process.
          </Typography>

        <Typography className={classes.gutterTop} gutterBottom align="justify" variant="h6">RAM</Typography>
          <Typography variant="body1" align="justify">
              The metric of memory used by a computational process. It is measured in <b>MBs</b> (megabyte seconds) which is calculated by adding spot RAM usages sampled every second for the  whole Time of execution (see metric Time).
          </Typography>
  
       <Typography className={classes.gutterTop} gutterBottom align="justify" variant="h6" id="max-ram">MaxRAM</Typography>
          <Typography variant="body1" align="justify">
              The largest RAM utilization in <b>MB</b> that occurred during execution of a computational process. It indicates a minimum RAM requirement of an execution environment running the process.
          </Typography>

        
        <Typography className={classes.gutterTop} gutterBottom align="justify" variant="h6" id="time">Time</Typography>
          <Typography variant="body1" align="justify">
             The wall time of the process execution measured in <b>seconds</b>.
          </Typography>

        <Typography className={classes.gutterTop} gutterBottom align="justify" variant="h6" id="net">NET</Typography>
          <Typography variant="body1" align="justify">
            The sum of data transferred and received via network interfaces during process execution, in <b>KB</b> (kilobytes);
          </Typography>

        <Typography className={classes.gutterTop} gutterBottom align="justify" variant="h6" id="net-time">NetTime</Typography>
          <Typography variant="body1" align="justify" >
              The average amount data transferred and received via network interfaces during process execution, in <b>KB/s</b> (kilobytes per second);
          </Typography>
          <br />
          <Divider></Divider>

          <Box mt={2}>
                <Copyright />
          </Box>

    </React.Fragment>
  )
}