import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import Container from "@material-ui/core/Container";
import CardMedia from "@material-ui/core/CardMedia";
import CircularProgress from "@material-ui/core/CircularProgress";
import Box from "@material-ui/core/Box";
import Link from "@material-ui/core/Link";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import Helmet from 'react-helmet';
import Copyright from "../commons/Copyright";

import ExplanationIcon from "mdi-material-ui/Compass";
import OutputIcon from "mdi-material-ui/WeatherCloudyArrowRight";
import InputIcon from "@material-ui/icons/Input";
import ServiceIcon from "mdi-material-ui/Ungroup";
import ResourceIcon from "@material-ui/icons/Toys";
import RewardIcon from "mdi-material-ui/GiftOutline";
import NunetIcon from "../../icons/IconsNunet10";
import DogIcon from "@material-ui/icons/Pets";
import StartTaskButton from "../commons/StartTask";
import IconTypography from "../commons/IconTypography";
import { UserContext } from "../utils/contexts";
import { render } from "react-dom";

import TermsofServiceIcon from "../../icons/IconsNunet10";
import { Dialog } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    gutterTop: {
        paddingTop: theme.spacing(2)
    },
    innerContainer: {
        padding: 0
    },
    half_divider: {
        width: "40%",
        margin: "0 auto"
    },
    under_nunet_icon: {
        marginTop: theme.spacing(-3)
    },
    nunet_icon: {
        height: "8rem",
        marginTop: "0rem"
    },
    no_text_transform: {
        textTransform: 'none'
    }
}));

function createData(arch, picture, breed, base64, time, discovered_times) {
    return {
        arch: base64 ? base64 : "architecture/" + arch,
        picture: base64 ? base64 : "breeds/" + picture,
        breed: breed,
        time: time,
        discovered_times: discovered_times + " times"
    };
}

const sampleOutput = createData(
    "nunet_demo_architecture.svg",
    "golden_retriever.jpeg",
    "Golden Retriever",
    null,
    "1 minute ago",
    6
);

export default function LegalTerms(props) {
    const classes = useStyles();

    const [loading, setLoading] = useState(true);
    const [isFirstLogIn, setFirstLogin] = useState(false);
    const [userInfo, setUserInfo] = React.useContext(UserContext);

    const paymentPerTask = 15;


    const goto_cookie_policy = () => {
        props.history.push('/legalTerms/coockiePolicy');
    }

    const goto_privacy_policy = () => {
        props.history.push('/legalTerms/privacyPolicy');
    }

    useEffect(() => {
        if (userInfo === false) {
            setLoading(false);
        } else if (userInfo) {
            setLoading(false);
        }
    }, [userInfo]);


    return (
        <React.Fragment>
            <TermsofServiceIcon className={classes.nunet_icon} />
            <Typography
                variant="h6"
                gutterBottom
                className={classes.under_nunet_icon}
            >
                Terms of Service
            </Typography>
            <Divider></Divider>

            <Typography
                variant="body1"
                className={classes.gutterTop}
                gutterBottom
                align="justify"
            >
                The application is provided 'AS IS' for demonstration purposes only,
                without warranty of any kind, express or implied,
                including but not limited to the warranties of merchantability,
                fitness for a particular purpose and non-infringement.
                In no event shall the authors or copyright holders be liable for any claim, damages or other liability,
                whether in an action of contract, tort of otherwise, arising from, out of or in connection with the applications
                and its software or the use or other dealings in the software.
        </Typography>
            <Divider></Divider>
            <Typography
                variant="body1"
                className={classes.gutterTop}
                gutterBottom
                align="justify"
            >

                NTXd tokens are designated for the demonstration purposes within the scope of this application only.
                NTXd DO NOT represent actually implemented crypto-currency tokens and WILL NOT be implemented in the future.
                The actual utility tokens of NuNet platform will be implemented as per development strategy independently of this demo application.
                The application may collect information about users.
                Information collected and cases of use when it is done so are described in Privacy Policy document.
                Accepting these terms of service implies that a user has read and accepted the privacy policy.
                The nature of information collected by the application depends on the chosen login method,
                where 'guest access' collects and stores the least personal information.

        </Typography>
            <Divider></Divider>

            <Typography
                variant="body1"
                className={classes.gutterTop}
                gutterBottom
                align="center"
            >

                <Button
                    className={classes.no_text_transform}
                    onClick={goto_cookie_policy}
                    variant="text"
                    color="primary">
                    Cookie Policy
               </Button>
                <span> | </span>
                <Button
                    className={classes.no_text_transform}
                    onClick={goto_privacy_policy}
                    variant="text"
                    color="primary"
                >
                    Privacy Policy
                    </Button>
            </Typography>

            <Typography
                variant="caption"
                align="center"
                className={classes.gutterTop}
            >
            <Box mt={2}>
                <Copyright />
            </Box>
        </Typography>
            <Helmet>
                <script type="text/javascript" async>
                    {`
                    (function (w,d) 
                    {var loader = function () 
                    {var s = d.createElement("script"), 
                    tag = d.getElementsByTagName("script")[0]; 
                    s.src="https://cdn.iubenda.com/iubenda.js"; 
                    tag.parentNode.insertBefore(s,tag);}; 
                    if(w.addEventListener){w.addEventListener("load", loader, false);}
                    else if(w.attachEvent){w.attachEvent("onload", loader);}
                    else{w.onload = loader;}})(window, document);
                    `}
                </script>

                <script type="text/javascript" async>
                    {`
                    (function (w,d) 
                    {var loader = function () {var s = d.createElement("script"), 
                    tag = d.getElementsByTagName("script")[0]; 
                    s.src="https://cdn.iubenda.com/iubenda.js"; 
                    tag.parentNode.insertBefore(s,tag);}; 
                    if(w.addEventListener){w.addEventListener("load", loader, false);}
                    else if(w.attachEvent){w.attachEvent("onload", loader);}
                    else{w.onload = loader;}})(window, document);
                `}
                </script>

            </Helmet>

        </React.Fragment>
    );
}
