import React from 'react'
import Iframe from 'react-iframe';

export default function PrivacyPolicy() {
    return (
        <Iframe url="https://www.iubenda.com/privacy-policy/67148905"
            scrolling="yes"
            height="1000px"
            width="100%"
        />
    )
}
