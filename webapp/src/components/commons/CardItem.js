import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import CardActionArea from "@material-ui/core/CardActionArea";
import Box from "@material-ui/core/Box";
import IconTypography from "./IconTypography";

/**
 * class styles and inline styles used
 * since box clone styles are being overriden
 * by most card component classes, very rude of them
 */
const useStyles = makeStyles(theme => ({
    content: {
        flex: "1 0 auto",
        padding: theme.spacing(0, 2)
    }
}));

export default function CardItem({ actionable = true, ...props }) {
    const classes = useStyles();

    return (
        <Box display="flex" my={1.2} mx={0} clone>
            <Card align="left" elevation={2}>
                <CardActionArea
                    disabled={!actionable}
                    onClick={props.onClick}
                    style={{ display: "flex" }}
                >
                    <CardMedia
                        image={props.image}
                        style={{ width: 100, height: 100 }}
                    />

                    <Box flex="1 0 auto" clone>
                        <CardContent className={classes.content}>
                            <IconTypography
                                variant="h6"
                                gutterBottom
                                icon={props.titleIcon}
                            >
                                {props.title}
                            </IconTypography>
                            <Box pt={2}>
                                {props.subtitleIcon ? (
                                    <IconTypography
                                        variant="subtitle2"
                                        color="textSecondary"
                                        icon={props.subtitleIcon}
                                    >
                                        {props.subtitle}
                                    </IconTypography>
                                ) : (
                                    // subtitle with no icon use bigger text variant
                                    <Typography variant="subtitle1">
                                        {props.subtitle}
                                    </Typography>
                                )}
                            </Box>
                        </CardContent>
                    </Box>
                </CardActionArea>
            </Card>
        </Box>
    );
}
