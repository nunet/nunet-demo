import React from "react";

import Link from "@material-ui/core/Link";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

export default function Copyright() {
    return (
        <Box mt={5} mb={1}>
            <Typography variant="body2" color="textSecondary" align="center">
                {"Copyright © "}
                <Link color="inherit" href="#">
                    NuNet
                </Link>{" "}
                {new Date().getFullYear()}
                {"."} {"All Rights Reserved."}
            </Typography>
        </Box>
    );
}
