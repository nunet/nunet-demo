import React, { useEffect, useState, useContext } from "react";
import { Route, Redirect } from "react-router-dom";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { Link as RouterLink } from "react-router-dom";
import { HashLink as HashLink} from 'react-router-hash-link';

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import Drawer from "@material-ui/core/Drawer";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import Link from "@material-ui/core/Link";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Avatar from "@material-ui/core/Avatar";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Box from "@material-ui/core/Box";

import HomeIcon from "@material-ui/icons/Home";
import MenuIcon from "@material-ui/icons/Menu";
import LogoutIcon from "@material-ui/icons/PowerSettingsNew";
import TimeLineIcon from "@material-ui/icons/TimelineRounded";
import AccountIcon from "@material-ui/icons/Person";
import DeviceIcon from "@material-ui/icons/Devices";
import RewardIcon from "mdi-material-ui/GiftOutline";
import HistoryIcon from "@material-ui/icons/History";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import ExecuteIcon from "@material-ui/icons/GraphicEqOutlined";
import InformationIcon from "mdi-material-ui/InformationOutline";
import LegalIcon from "mdi-material-ui/Cookie";
import HelpIcon from "@material-ui/icons/Help";
import LoginIcon from "@material-ui/icons/AccountCircle";

import { call_grpc, is_logged_in } from "../utils/grpc";
import { UserContext, SnackBarContext, CacheContext } from "../utils/contexts";
import Loading from "./Loading";
import get_device_info from "../utils/devices";

const grpc = require("grpc-web");
const { Empty } = require("google-protobuf/google/protobuf/empty_pb.js");
const { LogoutInput } = require("../../grpc/session_pb");

const useStyles = makeStyles(theme => ({
    title: {
        flexGrow: 1
    },
    drawerItem: {
        padding: 5
    },
    bigAvatar: {
        margin: 10,
        width: 60,
        height: 60
    }
}));

export default function Layout({
    component: Component,
    toggleTheme,
    ...props
}) {
    const classes = useStyles();
    const theme = useTheme();
    const showSnackBar = useContext(SnackBarContext);
    const [cache, updateCache] = useContext(CacheContext);

    const [openDrawer, setOpenDrawer] = React.useState(false);
    const [helpPath, setHelpPath] = React.useState("general-description");

    // user infos
    const [userInfo, setUserInfo] = useState(null);

    // loading state of logout action
    const [logoutLoading, setLogoutLoading] = useState(false);

    const toggleDrawer = open => event => {
        if (
            event.type === "keydown" &&
            (event.key === "Tab" || event.key === "Shift")
        ) {
            return;
        }

        setOpenDrawer(open);
    };

    function on_userInfo_error(error) {
        setUserInfo(false); // child components know that grpc error happens when userInfo is false but not null
    }

    function get_user_info() {
        let request = new Empty();
        call_grpc(
            "userInfo",
            request,
            user_info_handler,
            showSnackBar,
            on_userInfo_error
        );
    }

    function user_info_handler(response) {
        setUserInfo(response);
    }

    useEffect(() => {
        if (!userInfo && !props.unauthenticated) {
            get_user_info();
        }
        return () => {
            // reset cache if layout unmount for some reason
            updateCache({}, true);
        };
    }, [userInfo, props.unauthenticated]);

    const goBack = matchProps => () => {
        matchProps.history.goBack();
    };

    // demo-only navigation function
    const goto_rewards = matchProps =>
        function() {
            matchProps.history.push("/rewards");
            setHelpPath("reward-table");
        };

    const goto_consumer_perspective = matchProps =>
        function() {
            matchProps.history.push("/consumer");
            setHelpPath("consumer-perspective");
        };

    const goto_previous_tasks = matchProps =>
        function() {
            matchProps.history.push("/tasks");
            setHelpPath("task-history");
        };

    const goto_task_execute = matchProps =>
        function() {
            matchProps.history.push("/execute");
            setHelpPath("execute-task");
        };

    const goto_task_information = matchProps =>
        function() {
            matchProps.history.push("/info");
            setHelpPath("home");
        };

    const goto_provider_perspective = matchProps =>
        function() {
            matchProps.history.push("/provider");
            setHelpPath("provider-perspective");
        };

    const goto_legal_terms = matchProps =>
        function() {
            matchProps.history.push("/legalTerms");
            setHelpPath("");
        };

    const goto_help_page = matchProps =>
        function() {
            matchProps.history.push("/help");
            setHelpPath("");
        };

    function on_logout_error(err) {
        setLogoutLoading(false);
    }

    const on_logout_response = matchProps => response => {
        setLogoutLoading(false);

        if (response.getStatus() === grpc.StatusCode.OK) {
            // clear saved things all over the place
            localStorage.clear();
            updateCache({}, true);

            // redirect to login
            matchProps.history.push("/login");
        } else {
            showSnackBar("Error trying to logout. Please, try again");
        }
    };

    const logout = matchProps =>
        function(event) {
            // don't close drawer
            event.stopPropagation();

            setLogoutLoading(true);

            let request = new LogoutInput();
            request.setDeviceName(get_device_info());
            call_grpc(
                "logout",
                request,
                on_logout_response(matchProps),
                showSnackBar,
                on_logout_error
            );
        };

    return (
        <Route
            {...props}
            render={matchProps =>
                !is_logged_in() && !props.unauthenticated ? (
                    <Redirect to="/login" />
                ) : (
                    <React.Fragment>
                        <AppBar
                            position="fixed"
                            color={
                                theme.palette.type === "dark"
                                    ? "inherit"
                                    : "primary"
                            }
                        >
                            <Box pt={0.5} pb={0.5}>
                                <Toolbar>
                                    {/**
                                     * Toggle the top left icon between
                                     * Menu and Arrow back
                                     */
                                    (props.subpage || !is_logged_in() ? (
                                        <IconButton
                                            edge="start"
                                            color="inherit"
                                            aria-label="menu"
                                            onClick={goBack(matchProps)}
                                        >
                                            <ArrowBackIcon />
                                        </IconButton>
                                    ) : (
                                        <IconButton
                                            edge="start"
                                            color="inherit"
                                            aria-label="menu"
                                            onClick={toggleDrawer(true)}
                                        >
                                            <MenuIcon />
                                        </IconButton>
                                    ))}

                                    <Typography
                                        variant="h6"
                                        align="center"
                                        className={classes.title}
                                    >
                                        <img
                                            src="/logos/Nunet-text-dark-background.png"
                                            style={{
                                                height: "2.7rem",
                                                verticalAlign: "middle"
                                            }}
                                            alt="NuNet logo"
                                        />
                                    </Typography>

                                    {/* login link for non-logged user */}
                                    {!is_logged_in() ? (
                                        <IconButton
                                            edge="start"
                                            color="inherit"
                                            aria-label="menu"
                                        >
                                            <Link to={`/login`} component={RouterLink}>
                                                <LoginIcon />
                                            </Link>
                                        </IconButton>
                                    ) : (
                                    /* user balance and help link */
                                    userInfo && (
                                        <Box display="flex">
                                            <Box mt={1}>
                                                <Link
                                                    to={`/help#${helpPath}`}
                                                    component={HashLink}
                                                >
                                                    <HelpIcon />
                                                </Link>
                                            </Box>
                                            &nbsp;&nbsp;
                                            <Typography
                                                style={{
                                                    cursor: "pointer",
                                                    lineHeight: "initial"
                                                }}
                                                onClick={goto_consumer_perspective(
                                                    matchProps
                                                )}
                                                variant="subtitle1"
                                                align="right"
                                            >
                                                <b>
                                                    Balance
                                                    <br />
                                                    {userInfo.getBalance()}
                                                    NTXd
                                                </b>
                                            </Typography>
                                        </Box>
                                    ))}
                                </Toolbar>
                            </Box>
                        </AppBar>

                        {/* navigation drawer of app */}
                        <Drawer
                            open={openDrawer}
                            onClose={toggleDrawer(false)}
                            variant="temporary"
                        >
                            <div
                                className={classes.list}
                                role="presentation"
                                onClick={toggleDrawer(false)}
                                onKeyDown={toggleDrawer(false)}
                            >
                                <List>
                                    <Grid
                                        container
                                        justify="center"
                                        alignItems="center"
                                    >
                                        {userInfo &&
                                            (userInfo.getPicture() ? (
                                                <Avatar
                                                    className={
                                                        classes.bigAvatar
                                                    }
                                                    alt="User Photo"
                                                    src={userInfo.getPicture()}
                                                />
                                            ) : (
                                                <Avatar
                                                    className={
                                                        classes.bigAvatar
                                                    }
                                                >
                                                    <AccountIcon fontSize="large" />
                                                </Avatar>
                                            ))}
                                    </Grid>
                                    <Grid
                                        container
                                        justify="center"
                                        className={classes.drawerItem}
                                        alignItems="center"
                                    >
                                        {userInfo && (
                                            <Typography variant="subtitle1">
                                                {userInfo.getIsGuest()
                                                    ? "Guest"
                                                    : userInfo.getEmail()}
                                            </Typography>
                                        )}
                                    </Grid>
                                    <Grid
                                        container
                                        justify="center"
                                        // className={classes.drawerItem}
                                        alignItems="center"
                                        style={{ marginBottom: 16 }}
                                    >
                                        {userInfo && (
                                            <Typography variant="caption">
                                                {userInfo.getBalance()} NTXd
                                            </Typography>
                                        )}
                                    </Grid>
                                    <Divider />
                                    <ListItem
                                        button
                                        onClick={goto_task_information(
                                            matchProps
                                        )}
                                    >
                                        <ListItemIcon>
                                            <HomeIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Home" />
                                    </ListItem>
                                    <ListItem
                                        button
                                        onClick={goto_task_execute(matchProps)}
                                    >
                                        <ListItemIcon>
                                            <ExecuteIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Execute Task" />
                                    </ListItem>

                                    <ListItem
                                        button
                                        onClick={goto_rewards(matchProps)}
                                    >
                                        <ListItemIcon>
                                            <RewardIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Reward Table" />
                                    </ListItem>

                                    <ListItem
                                        button
                                        onClick={goto_previous_tasks(
                                            matchProps
                                        )}
                                    >
                                        <ListItemIcon>
                                            <HistoryIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Task History" />
                                    </ListItem>
                                    <ListItem
                                        button
                                        onClick={goto_consumer_perspective(
                                            matchProps
                                        )}
                                    >
                                        <ListItemIcon>
                                            <TimeLineIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Consumer Perspective" />
                                    </ListItem>
                                    <ListItem
                                        button
                                        onClick={goto_provider_perspective(
                                            matchProps
                                        )}
                                    >
                                        <ListItemIcon>
                                            <DeviceIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Provider Perspective" />
                                    </ListItem>

                                    <ListItem
                                        button
                                        onClick={goto_legal_terms(matchProps)}
                                    >
                                        <ListItemIcon>
                                            <LegalIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Legal Terms" />
                                    </ListItem>

                                    <ListItem
                                        button
                                        onClick={goto_help_page(matchProps)}
                                    >
                                        <ListItemIcon>
                                            <HelpIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Help" />
                                    </ListItem>

                                    <ListItem
                                        button
                                        onClick={logout(matchProps)}
                                        disabled={logoutLoading}
                                    >
                                        <ListItemIcon>
                                            {logoutLoading ? (
                                                <Loading pt={0} size={20} /> // pt={0} to override default padding
                                            ) : (
                                                <LogoutIcon />
                                            )}
                                        </ListItemIcon>
                                        <ListItemText primary="Logout" />
                                    </ListItem>
                                </List>
                            </div>
                        </Drawer>
                        <UserContext.Provider value={[userInfo, setUserInfo]}>
                            <Box pt={9}>
                                {" "}
                                {/* padding to not hide in app bar */}
                                <Container
                                    align="center"
                                    maxWidth="sm" // if desktop protect layout
                                >
                                    <Component {...matchProps} />
                                </Container>
                            </Box>
                        </UserContext.Provider>
                    </React.Fragment>
                )
            }
        />
    );
}
