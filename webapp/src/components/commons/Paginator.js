import React from "react";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import Pagination from "material-ui-flat-pagination";
import ArrowLeftIcon from "@material-ui/icons/ArrowLeft";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import teal from "@material-ui/core/colors/teal";

const theme = createMuiTheme({
    palette: {
        primary: teal,
        secondary: teal,
        action: { disabled: teal[800] }
    }
});

function Paginator(props) {
    function handleClick(offset) {
        props.paginate(offset);
    }

    return (
        <MuiThemeProvider theme={theme}>
            <Pagination
                limit={props.limit}
                offset={props.offset}
                total={props.total}
                reduced={true}
                previousPageLabel={<ArrowLeftIcon />}
                nextPageLabel={<ArrowRightIcon />}
                onClick={(e, offset) => handleClick(offset)}
            />
        </MuiThemeProvider>
    );
}

export default Paginator;
