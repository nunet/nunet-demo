import React from "react";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { amber, green } from "@material-ui/core/colors";

import ErrorIcon from "@material-ui/icons/Error";
import InfoIcon from "@material-ui/icons/Info";
import WarningIcon from "@material-ui/icons/Warning";
import SuccessIcon from "@material-ui/icons/CheckCircle";

const variantIcons = {
    success: SuccessIcon,
    warning: WarningIcon,
    error: ErrorIcon,
    info: InfoIcon
};

const variantColors = {
    success: green[600],
    error: "error.dark", // theme color
    info: "text.secondary", // theme color
    warning: amber[700]
};

export default function ShowInfo({ variant='info', ...props }) {
    // defaults to info variant

    const Icon = variantIcons[variant];
    const color = variantColors[variant];

    return (
        <Box color={color} my={1} {...props}>
            <Icon fontSize="large" />
            <Typography gutterBottom variant="subtitle1">
                {props.children}
            </Typography>
        </Box>
    );
}
