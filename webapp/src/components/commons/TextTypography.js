import React from "react";

import Box from "@material-ui/core/Box";
import { styled } from "@material-ui/core/styles";
import { compose, spacing } from "@material-ui/system";
import Typography from "@material-ui/core/Typography";

function TextTypography({ icon: Icon, ...props }) {
    return (
        <Typography {...props}>
            {props.children}
        </Typography>
    );
}

/**
 * Incorporate material-ui system declarative approach of styling, haleluya!!
 */
export default styled(TextTypography)(compose(spacing));
