import React, { useState, useEffect } from "react";

import Typography from "@material-ui/core/Typography";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Box from "@material-ui/core/Box";

import CoinIcon from "mdi-material-ui/Coins";
import AssignmentIcon from "@material-ui/icons/Assignment";
import ConsumerIcon from "../../icons/IconsNunet04";
import NunetHeading from "../commons/NunetHeading";
import IconTypography from "../commons/IconTypography";
import Loading from "../commons/Loading";
import PaperTable from "../commons/PaperTable";
import StartTaskButton from "../commons/StartTask";
import { UserContext } from "../utils/contexts";
import Copyright from "../commons/Copyright";

function ConsumerPerspective(props) {
    const [loading, setLoading] = useState(true);
    const [userInfo, setUserInfo] = React.useContext(UserContext);

    const paymentPerTask = 15;

    useEffect(() => {
        if (userInfo === false) {
            setLoading(false);
        } else if (userInfo) {
            setLoading(false);
        }
        return () => {};
    }, [userInfo]);

    return (
        <React.Fragment>
            <NunetHeading icon={ConsumerIcon}>
                Consumer Perspective
            </NunetHeading>

            {loading ? (
                <Loading />
            ) : (
                userInfo && (
                    <React.Fragment>
                        <Box>
                            <Typography gutterBottom variant="subtitle1">
                                Your balance allows to run{" "}
                                {Math.floor(
                                    userInfo.getBalance() / paymentPerTask
                                )}{" "}
                                computational tasks on NuNet infrastructure
                            </Typography>
                        </Box>

                        <Box>
                            <StartTaskButton
                                gutterBottom
                                history={props.history}
                            />
                        </Box>

                        <IconTypography
                            gutterBottom
                            variant="h6"
                            icon={CoinIcon}
                            align="left"
                            mt={2}
                            pl={1}
                        >
                            Your Tokens
                        </IconTypography>

                        <PaperTable>
                            <Table aria-label="simple table">
                                <TableBody>
                                    <TableRow>
                                        <TableCell>Current Balance</TableCell>
                                        <TableCell>
                                            {userInfo.getBalance()}NTXd
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell>Token spent</TableCell>
                                        <TableCell>
                                            {userInfo.getTotalTokenSpent()}NTXd
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell>Total reward</TableCell>
                                        <TableCell>
                                            {userInfo.getTotalReward()}NTXd
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell>Net earned</TableCell>
                                        <TableCell>
                                            {userInfo.getTotalReward() -
                                                userInfo.getTotalTokenSpent()}
                                            NTXd
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </PaperTable>

                        <IconTypography
                            gutterBottom
                            variant="h6"
                            icon={AssignmentIcon}
                            align="left"
                            pl={1}
                        >
                            Your Tasks
                        </IconTypography>

                        <PaperTable>
                            <Table aria-label="simple table">
                                <TableBody>
                                    <TableRow>
                                        <TableCell>Tasks run</TableCell>
                                        <TableCell>
                                            {userInfo.getTaskRun()}
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell>Completed tasks</TableCell>
                                        <TableCell>
                                            {userInfo.getCompletedTask()}
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell>Failed tasks</TableCell>
                                        <TableCell>
                                            {userInfo.getTaskRun() -
                                                userInfo.getCompletedTask()}
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </PaperTable>
                    </React.Fragment>
                )
            )}
            <Box mt={5}>
                <Copyright />
            </Box>
        </React.Fragment>
    );
}

export default ConsumerPerspective;
