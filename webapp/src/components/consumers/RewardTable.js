import React, { useState, useEffect, useContext } from "react";

import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

import RewardIcon from "mdi-material-ui/GiftOutline";
import DogIcon from "@material-ui/icons/Pets";

import { call_grpc } from "../utils/grpc";
import { base64_src } from "../utils/images";
import IconTypography from "../commons/IconTypography";
import Loading from "../commons/Loading";
import CardItem from "../commons/CardItem";
import Paginator from "../commons/Paginator";
import { CacheContext, SnackBarContext } from "../utils/contexts";
import ShowInfo from "../commons/ShowInfo";
import ExecuteTask from "../commons/StartTask";
import Copyright from "../commons/Copyright";

const { PageInput } = require("../../grpc/session_pb");

export default function RewardTable(props) {
    const itemsPerPage = 5;
    const totalItems = 132;

    const showSnackBar = useContext(SnackBarContext);
    const [cache, updateCache] = useContext(CacheContext);

    const [loading, setLoading] = useState(false);

    const [rewards, setRewards] = useState(cache.rewards || null);
    const [offset, setOffset] = useState(0);

    function on_grpc_error(err) {
        setLoading(false);
    }

    function parseGrpcReward(reward) {
        return {
            base64: reward.getBase64(),
            breed_name: reward.getBreedName(),
            reward: reward.getReward()
        };
    }

    function clean_breed_name(breed_name) {
        return breed_name.replace("_", " ");
    }

    function on_rewards_response(response) {
        setLoading(false);
        if (response.getRewardsList().length > 0) {
            let parsed_rewards = response.getRewardsList().map(parseGrpcReward);
            setRewards(rewards =>
                rewards ? [...rewards, ...parsed_rewards] : parsed_rewards
            );
        } else if (rewards === null) {
            // no rewards found should be empty array, but rewards stay null if error happens
            setRewards([]);
        }
    }

    function get_rewards() {
        setLoading(true);
        let request = new PageInput();
        request.setOffset(offset);
        request.setSize(itemsPerPage);

        call_grpc(
            "rewardTable",
            request,
            on_rewards_response,
            showSnackBar,
            on_grpc_error
        );
    }

    function paginate(offset) {
        setOffset(offset);
        setRewards(null); //! prior items should be cached, not discarded
    }

    useEffect(() => {
        if (!rewards) {
            get_rewards();
        }

        return () => {
            updateCache({ rewards: rewards });
        };
    }, [rewards]);

    return (
        <React.Fragment>
            <IconTypography variant="h5" gutterBottom icon={RewardIcon}>
                Bounty Rewards
            </IconTypography>

            <Typography>
                You will be rewarded the indicated amount of NTXd tokens if a
                corresponding dog breed is detected in your submitted image.
            </Typography>

            {rewards &&
                Object.keys(rewards).map(index => (
                    <CardItem
                        key={index}
                        image={base64_src(rewards[index].base64)}
                        title={clean_breed_name(rewards[index].breed_name)}
                        titleIcon={DogIcon}
                        subtitle={rewards[index].reward + "NTXd"}
                        subtitleIcon={null}
                        actionable={false}
                    />
                ))}

            {/* show loading indicator */}
            {loading && <Loading py={20} />}

            <Box>
                <Paginator
                    limit={itemsPerPage}
                    total={totalItems}
                    paginate={paginate}
                    offset={offset}
                />
                <ExecuteTask history={props.history} />
            </Box>

            {!loading && rewards && rewards.length === 0 && (
                <React.Fragment>
                    <Box mt={3}>
                        <ShowInfo>
                            There are no rewards data available for now!
                        </ShowInfo>
                        <ExecuteTask history={props.history} />
                    </Box>
                </React.Fragment>
            )}
            <Box mt={5}>
                <Copyright />
            </Box>
        </React.Fragment>
    );
}
