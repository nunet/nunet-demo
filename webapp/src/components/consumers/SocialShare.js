import React, { useContext } from "react";
import Box from "@material-ui/core/Box";

import { Twitter, Linkedin, Telegram, Facebook } from "react-social-sharing";
import { FacebookShareButton } from "react-share";

import { SnackBarContext } from "../utils/contexts";
import Copyright from "../commons/Copyright";
import routes from "../utils/routes";

export default function SocialMediaShare(props) {
    const { shareToken } = props;
    const public_url = process.env.REACT_APP_ROOT_URL;
    const showSnackBar = useContext(SnackBarContext);

    const text =
        "I just ran an AI task using a demo app of NuNet platform: a global economy of decentralized computing";
    const mediaurl = {
        twitter: "https://nunet.io   @nunet_global",
        telegram: "https://nunet.io   https://t.me/NuNet_community",
        linkedin:
            "https://nunet.io   https://www.linkedin.com/company/nunet-global",
        facebook: "https://nunet.io   https://www.facebook.com/NunetGlobal"
    };

    let share_url = null;
    if (shareToken && public_url) {
        share_url = `${public_url}${routes.shared}?id=${shareToken}`; // no slash needed between public_url and routes.shared
    } else {
        if (!shareToken) {
            showSnackBar("Could not share current task for now.", "warning");
        }

        if (!public_url) {
            showSnackBar("Sharing feature is not working for now.", "warning");
        }

        return <Box></Box>;
    }

    return (
        <div>
            <Box display="flex">
                <Twitter
                    message={`${text} \n${mediaurl.twitter} \n`}
                    link={share_url}
                    solid
                    small
                />
                <Telegram
                    link={`${text}\n\n${mediaurl.telegram}\n\n${share_url}`}
                    solid
                    small
                />
                <Linkedin
                    message={`${text} \n${mediaurl.linkedin} \n`}
                    link={share_url}
                    solid
                    small
                />
                <FacebookShareButton
                    url={share_url}
                    quote={`${text} \n${mediaurl.facebook} \n`}
                >
                    <Facebook solid small />
                </FacebookShareButton>
            </Box>
        </div>
    );
}
