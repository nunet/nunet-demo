import React, { useState, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepContent from "@material-ui/core/StepContent";
import CircularProgress from "@material-ui/core/CircularProgress";
import Box from "@material-ui/core/Box";
import StepButton from "@material-ui/core/StepButton";

import CaptureIcon from "@material-ui/icons/PhotoCamera";
import ServiceIcon from "@material-ui/icons/CallMade";
import UploadIcon from "@material-ui/icons/CloudUpload";
import DogIcon from "mdi-material-ui/DogSide";
import YoloIcon from "mdi-material-ui/Ungroup";
import CntkIcon from "mdi-material-ui/Paw";
import ExecuteIcon from "../../icons/IconsNunet08";
import ResultIcon from "mdi-material-ui/CloudTags";
import LogIcon from "mdi-material-ui/Console";
import StatIcon from "@material-ui/icons/Equalizer";
import InfoIcon from "@material-ui/icons/InfoOutlined";

import { TAGS, show_grpc_error, get_local_token } from "../utils/grpc";
import get_image_base64, { base64_src } from "../utils/images";
import Terminal from "../commons/Terminal";
import Panel from "../commons/Panel";
import NunetHeading from "../commons/NunetHeading";
import ShowInfo from "../commons/ShowInfo";
import { UserContext, CacheContext, SnackBarContext } from "../utils/contexts";
import IconTypography from "../commons/IconTypography";
import Copyright from "../commons/Copyright";
import routes from "../utils/routes";

const grpc = require("grpc-web");
const { ExecutionInput } = require("../../grpc/session_pb");
const { client } = require("../utils/grpc");

const useStyles = makeStyles(theme => ({
    // stepper styles down here
    stepper_root: {
        width: "100%"
    },
    stepper: {
        // similarize with background color
        backgroundColor: theme.palette.background.default,

        // make more room for content in small screens
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2)
    },
    paper_image: {
        display: "block"
    },
    step_icon: {
        verticalAlign: "sub",
        marginRight: theme.spacing(1)
    },
    step_info_icon: {
        verticalAlign: "text-bottom",
        marginLeft: theme.spacing(1.2),
        color: theme.palette.text.secondary
    }
}));

function getSteps() {
    return [
        "Select a dog image",
        "YOLO object detection",
        "Dog breed detection"
    ];
}

const STATS = {
    CPU: "cpu_usage",
    MEMORY: "memory_usage",
    TOTAL_MEMORY: "total_memory",
    TIME: "time_taken",
    NET: "net_rx"
};

export default function TaskExecute(props) {
    const classes = useStyles();

    const showSnackBar = useContext(SnackBarContext);
    const [cache, updateCache] = useContext(CacheContext);
    const [userInfo, setUserInfo] = useContext(UserContext);
    const STEP = {
        SELECT: 0,
        YOLO: 1,
        CNTK: 2
    };

    const step_icons = [
        <DogIcon className={classes.step_icon} />,
        <YoloIcon className={classes.step_icon} />,
        <CntkIcon className={classes.step_icon} />
    ];

    const [image_file, setImage_file] = useState(null);
    const [selectedBase64, setSelectedBase64] = useState(null);
    const [result_id, setResultId] = useState(null);

    const [stepContents, setStepContents] = useState({
        [STEP.SELECT]: { result: null, bad: null },
        [STEP.QUEUE]: { result: null, bad: null },
        [STEP.YOLO]: { result: null, bad: null },
        [STEP.CNTK]: { result: null, bad: null }
    });
    const [stepsImages, setStepsImages] = useState({
        [STEP.SELECT]: null,
        [STEP.YOLO]: null,
        [STEP.CNTK]: null
    });
    const [stepLines, setStepLines] = useState({
        [STEP.YOLO]: [],
        [STEP.CNTK]: []
    });
    const [stepsStats, setStepsStats] = useState({
        [STEP.YOLO]: null,
        [STEP.CNTK]: null
    });

    // stepper logic states
    const [activeStep, setActiveStep] = useState(0);
    const [completedSteps, setCompletedSteps] = React.useState({});
    const [onwork, setOnwork] = React.useState({});
    const [errorSteps, setErrorSteps] = React.useState({});
    const [disabledSteps, setDisabledSteps] = React.useState({});

    const steps = getSteps();

    const handleStep = step => () => {
        setActiveStep(step);
    };

    const goto_result = (task_id, seconds, task_index) => () => {
        props.history.push({
            pathname: `${routes.task_result}`,
            timestamp: seconds,
            task_identifier: task_index,
            search: `?id=${task_id}`
        });
    };

    function enableFileSelectionStep() {
        setDisabledSteps(disabledSteps => ({
            ...disabledSteps,
            [STEP.SELECT]: false
        }));
    }

    /**
     * Callback when yolo execution step starts
     */
    function onYoloStart() {
        // change active step to yolo
        setActiveStep(STEP.YOLO);

        // set onwork for yolo step
        setOnwork({ [STEP.SELECT]: false, [STEP.YOLO]: true });

        // set complete for prior step
        setCompletedSteps({ [STEP.SELECT]: true });

        // also disable file selecting step
        setDisabledSteps({ [STEP.SELECT]: true });
    }

    /**
     * Callback when cntk execution step starts
     */
    function onCntkStart() {
        // change active step to cntk
        setActiveStep(STEP.CNTK);

        // set onwork for cntk step
        setOnwork({
            [STEP.SELECT]: false,
            [STEP.YOLO]: false,
            [STEP.CNTK]: true
        });

        // set complete for prior steps
        setCompletedSteps({ [STEP.SELECT]: true, [STEP.YOLO]: true });
    }

    /**
     * Reset all execution steps states
     */
    function resetExecutionSteps() {
        // reset result contents
        setStepContents({
            [STEP.SELECT]: { result: null, bad: null },
            [STEP.QUEUE]: { result: null, bad: null },
            [STEP.YOLO]: { result: null, bad: null },
            [STEP.CNTK]: { result: null, bad: null }
        });

        // reset error steps
        setErrorSteps({});

        // reset completed stpes
        setCompletedSteps({});

        // reset result images
        setStepsImages(stepsImages => ({
            ...stepsImages,
            [STEP.YOLO]: null,
            [STEP.CNTK]: null
        }));

        // reset resource statistics
        setStepsStats(stepsStats => ({
            [STEP.SELECT]: null,
            [STEP.YOLO]: null,
            [STEP.CNTK]: null
        }));

        // reset docker logs
        setStepLines({
            [STEP.YOLO]: [],
            [STEP.CNTK]: []
        });

        // reset task result link
        setResultId(null);
    }

    /**
     * Callback when stat recieved from grpc stream
     * @param {str} step : on which exection step : one of STEP.SELECT, STEP.YOLO, STEP.CNTK
     * @param {str} stat : stat json object stringified
     */
    // ! assumes execution step ends when stat recieved
    function onStatRecieve(step, stat) {
        // remove progress indicator
        setOnwork({ [step]: false });

        // set step is completed
        setCompletedSteps(completedSteps => ({
            ...completedSteps,
            [step]: true
        }));

        setUserInfo(null); // reset user info to reload like balance, and other user infos

        try {
            let stat_json = JSON.parse(stat);
            setStepsStats(stepStats => ({
                ...stepStats,
                [step]: stat_json
            }));
        } catch (err) {
            setStepsStats(stepStats => ({
                ...stepStats,
                [step]: {}
            }));
        }

        // if last stat retrieved
        if (step === STEP.CNTK) {
            // enable file selecting step
            enableFileSelectionStep();
        }
    }

    /**
     * Callback when log recieved from grpc stream
     * @param {str} step : on which exection step : one of STEP.SELECT, STEP.YOLO, STEP.CNTK
     * @param {str} log : docker log line from grpc server
     */
    function onLogRecieve(step, log) {
        setStepLines(stepLines => ({
            ...stepLines,
            [step]: [...stepLines[step], log]
        }));
    }

    /**
     * Callback when result recieved from grpc stream
     * @param {str} step : on which exection step : one of STEP.SELECT, STEP.YOLO, STEP.CNTK
     * @param {str} result : result string of the execution step, could illustrate success only
     */
    function onGoodResultRecieve(step, result) {
        setStepContents(stepContents => ({
            ...stepContents,
            [step]: { result: result }
        }));
    }


    /**
     * Callback when result recieved from grpc stream
     * @param {str} step : on which exection step : one of STEP.SELECT, STEP.YOLO, STEP.CNTK
     * @param {str} result : result string of the execution step, could illustrate success only
     */
    function onQueue(step, result) {
        setStepContents(stepContents => ({
            ...stepContents,
            [step]: { result: result }
        }));
    }

    /**
     * Callback when bad result recieved from grpc stream
     * @param {str} step : on which exection step : one of STEP.SELECT, STEP.YOLO, STEP.CNTK
     * @param {str} bad_result : result string but illustrating failure only
     */
    function onBadResultRecieve(step, bad_result) {
        // remove progress indicator
        setOnwork({ [step]: false });

        // consider bad result as an error
        setErrorSteps(errorSteps => ({
            ...errorSteps,
            [step]: bad_result
        }));

        // ? consider bad result as a result
        // setStepContents(stepContents => ({
        //     ...stepContents,
        //     [step]: { result: bad_result, bad: true }
        // }));

        // enable file selecting step
        enableFileSelectionStep();
    }

    /**
     * Callback when output image recieved from grpc stream
     * @param {str} step : on which exection step : one of STEP.SELECT, STEP.YOLO, STEP.CNTK
     * @param {str} output_image : output base64 image string for execution step
     */
    function onOutputImageRecieve(step, output_image) {
        setStepsImages(stepsImages => ({
            ...stepsImages,
            [step]: output_image
        }));
    }

    /**
     * Call grpc service with selected dog image and start execution
     */
    function call_service() {
        // reset a task may have been executed before
        resetExecutionSteps();

        // make selecting step on work
        setOnwork({ [STEP.SELECT]: true });

        // setup execute request
        let execute_request = new ExecutionInput();
        execute_request.setBase64(selectedBase64);

        let token = get_local_token();
        let stream = client.execute(execute_request, { access_token: token });

        let step = STEP.SELECT; // step variable detects at which step is the stream
        stream.on("data", function (response) {
            // if data recieved for first time
            if (step === STEP.SELECT) {
                step = STEP.YOLO; // transition from File selecting step to yolo
                onYoloStart();

                updateCache({ tasks: null }); // invalidate tasks cache
            }

            // extract data from response
            let log_info = response.getLogInfo();
            let tag = response.getTag();
            switch (tag) {
                case TAGS.log:
                    onLogRecieve(step, log_info);
                    break;
                case TAGS.yolo_result:
                case TAGS.cntk_result:
                    onGoodResultRecieve(step, log_info);
                    break;
                case TAGS.bad_result:
                    onBadResultRecieve(step, log_info);
                    break;
                case TAGS.error:
                    showSnackBar(log_info);

                    // enable file selecting step
                    enableFileSelectionStep();
                    break;
                case TAGS.next: // ! assumes next is sent from yolo to cntk transition
                    step = STEP.CNTK;
                    onCntkStart();
                    break;
                case TAGS.yolo_stat:
                case TAGS.cntk_stat:
                    onStatRecieve(step, log_info);
                    window.scrollTo(0, document.body.scrollHeight);
                    break;
                case TAGS.yolo_output:
                    onOutputImageRecieve(step, log_info);
                    window.scrollTo(0, document.body.scrollHeight);
                    break;
                case TAGS.task_id:
                    setResultId(log_info);
                    break;
                case TAGS.on_queue:
                    onQueue(step, log_info);
                    break;
                default:
                    console.log("unknown tag: ", tag);
            }
        });

        stream.on("status", function (status) {
            if (status.code !== grpc.StatusCode.OK) {
                setOnwork({ [step]: false });
                if (step !== STEP.SELECT) {
                    // show error inside stepper
                    const error_shower = message => {
                        setErrorSteps({ [step]: message });
                    };

                    show_grpc_error(status.code, status.details, error_shower);

                    enableFileSelectionStep();
                } else {
                    show_grpc_error(status.code, status.details, showSnackBar);
                }
                console.log("status err details:", status.details);
            }

            console.log("status: ", status.code);
            console.log("status: ", status.details);
            console.log("status: ", status.metadata);
        });

        stream.on("error", function (err) {
            setOnwork({ [step]: false });

            show_grpc_error(err.code, err.message, showSnackBar);
            console.log("err: ", err);
        });

        stream.on("end", function (end) {
            setOnwork({ [step]: false });
            // stream end signal
            console.log("stream: end");
        });
    }

    async function handleImageUpload(event) {
        if (event.target.files.length === 1) {
            setImage_file(event.target.files[0]);

            let base64_image = await get_image_base64(
                event.target.files[0],
                "selected_image",
                showSnackBar
            );
            setSelectedBase64(base64_image);
        } else {
            console.warn("no file selected");
        }
    }

    function getStepJSX(step) {
        switch (step) {
            case STEP.SELECT:
                return (
                    <Box my={1}>
                        <Grid container>
                            <Grid item xs>
                                <Box mb={1}>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        startIcon={<CaptureIcon />}
                                        component="label"
                                        id="capture-button"
                                    >
                                        <span id="capture-text">Capture</span>
                                        <input
                                            type="file"
                                            accept="image/*"
                                            capture
                                            style={{ display: "none" }}
                                            onChange={handleImageUpload}
                                            id="capture-input-file"
                                        />
                                    </Button>
                                </Box>
                            </Grid>
                            <Grid item xs>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    startIcon={<UploadIcon />}
                                    component="label"
                                    id="upload-button"
                                >
                                    <span id="upload-text">Upload</span>
                                    <input
                                        type="file"
                                        accept="image/*"
                                        style={{ display: "none" }}
                                        onChange={handleImageUpload}
                                        id="upload-input-file"
                                    />
                                </Button>
                            </Grid>
                            {(image_file || selectedBase64) && (
                                <React.Fragment>
                                    <Grid item xs={12}>
                                        <Box mt={2}>
                                            <Paper id="selected-image">
                                                <img
                                                    src={URL.createObjectURL(
                                                        image_file
                                                    )}
                                                    width="100%"
                                                    alt="Seleted"
                                                    className={
                                                        classes.paper_image
                                                    }
                                                />
                                            </Paper>
                                        </Box>
                                        <Typography
                                            component="p"
                                            variant="caption"
                                            id="selected-image-text"
                                        >
                                            Selected Image
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={12}>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            startIcon={<ServiceIcon />}
                                            onClick={call_service}
                                            component="label"
                                            disabled={onwork[0]}
                                            style={{ marginTop: 16 }}
                                            id="calling-service-button"
                                        >
                                            {onwork[0]
                                                ? "Calling Service..."
                                                : "Call Service"}
                                        </Button>
                                    </Grid>
                                </React.Fragment>
                            )}
                        </Grid>
                    </Box>
                );
            case STEP.YOLO:
            case STEP.CNTK:
                // if step has not started yet
                if (
                    !onwork[step] &&
                    !completedSteps[step] &&
                    !errorSteps[step]
                ) {
                    return (
                        <Box my={1}>
                            <ShowInfo id={`step-${step}-info-text-1`}>Service not called yet!</ShowInfo>
                            <Typography variant="caption" color="textSecondary" id={`step-${step}-info-text-2`}>
                                Prior step must finish successfully.
                            </Typography>
                        </Box>
                    );
                } else {
                    return (
                        <React.Fragment>
                            {/* Docker logs if docker log line found */}
                            {stepLines[step].length > 0 && (
                                <Box my={1}>
                                    <IconTypography
                                        icon={LogIcon}
                                        gutterBottom
                                        variant="subtitle1"
                                        align="left"
                                        mt={3}
                                        pl={1}
                                        id={`step-${step}-docker-logs`}
                                    >
                                        <span id={`step-${step}-docker-log-header-text`}>Docker Logs</span>
                                    </IconTypography>
                                    <Terminal lines={stepLines[step]} id={`step-${step}-terminal`} step={step}/>
                                </Box>
                            )}

                            {/* Resource statistics */}
                            {stepsStats[step] &&
                                (Object.keys(stepsStats[step]).length === 0 ? (
                                    // couldnot get resources statistics because of statistics json parsing error */
                                    <Panel variant="error" id={`step-1-${step}couldnot-get-resource`}>
                                        Couldnot get resources statistics!
                                    </Panel>
                                ) : (
                                        <Box mt={2} mb={2}>
                                            <IconTypography
                                                icon={StatIcon}
                                                gutterBottom
                                                variant="subtitle1"
                                                align="left"
                                                mt={3}
                                                pl={1}        
                                            >
                                                <span id={`step-${step}-resource-usage-text`}>Resource Usage</span>
                                        </IconTypography>
                                            <Grid container spacing={2}>
                                                <Grid item sm={4} xs={6}>
                                                    <Typography variant="body2" id={`step-${step}-total-cpu-text`}>
                                                        Total CPU
                                                </Typography>
                                                    <Box mb={2}>
                                                        <Typography variant="body2">
                                                            <span id={`step-${step}-total-cpu-value`}>
                                                            {stepsStats[step][
                                                                STATS.CPU
                                                            ].toFixed(2)}{" "}
                                                            </span>
                                                            <span id={`step-${step}-total-cpu-unit`}>Mticks</span>
                                                    </Typography>
                                                    </Box>
                                                </Grid>

                                                <Grid item sm={4} xs={6}>
                                                    <Typography variant="body2" id={`step-${step}-ram-text`}>
                                                        RAM
                                                </Typography>
                                                    <Box mb={2}>
                                                        <Typography variant="body2">
                                                            <span id={`step-${step}-ram-value`}>
                                                            {stepsStats[step][
                                                                STATS.TOTAL_MEMORY
                                                            ].toFixed(2)}{" "}
                                                            </span>
                                                            <span id={`step-${step}-ram-unit`}>MBs</span>
                                                    </Typography>
                                                    </Box>
                                                </Grid>

                                                <Grid item sm={4} xs={6}>
                                                    <Typography variant="body2" id={`step-${step}-max-ram-text`}>
                                                        Max RAM
                                                </Typography>
                                                    <Box mb={2}>
                                                        <Typography variant="body2">
                                                            <span id={`step-${step}-max-ram-value`}>
                                                            {stepsStats[step][
                                                                STATS.MEMORY
                                                            ].toFixed(2)}{" "}
                                                            </span>
                                                            <span id={`step-${step}-max-ram-unit`}>MB</span>
                                                    </Typography>
                                                    </Box>
                                                </Grid>
                                                <Grid item sm={4} xs={6}>
                                                    <Typography variant="body2" id={`step-${step}-time-text`}>
                                                        TIME
                                                </Typography>
                                                    <Box mb={2}>
                                                        <Typography variant="body2">
                                                            <span id={`step-${step}-time-value`}>
                                                            {stepsStats[step][
                                                                STATS.TIME
                                                            ].toFixed(4)}{" "}
                                                            </span>
                                                            <span id={`step-${step}-time-unit`}>s</span>
                                                    </Typography>
                                                    </Box>
                                                </Grid>

                                                <Grid item sm={4} xs={6}>
                                                    <Typography variant="body2" id={`step-${step}-net-text`}>
                                                        NET
                                                </Typography>
                                                    <Box mb={2}>
                                                        <Typography variant="body2">
                                                            <span id={`step-${step}-net-value`}>
                                                            {stepsStats[step][
                                                                STATS.NET
                                                            ].toFixed(2)
                                                            }{" "}
                                                            </span>
                                                            <span id={`step-${step}-net-unit`}>KB</span>
                                                    </Typography>
                                                    </Box>
                                                </Grid>

                                                <Grid item sm={4} xs={6}>
                                                    <Typography variant="body2" id={`step-${step}-net-time-text`}>
                                                        NetTime
                                                </Typography>
                                                    <Box mb={2}>
                                                        <Typography variant="body2">
                                                            <span id={`step-${step}-net-time-value`}>
                                                            {(
                                                                stepsStats[step][
                                                                    STATS.NET
                                                                ].toFixed(2) /
                                                                stepsStats[step][
                                                                    STATS.TIME
                                                                ].toFixed(2)
                                                            ).toFixed(2)
                                                            }{" "}
                                                            </span>
                                                            <span id={`step-${step}-net-time-unit`}>KB/s</span>
                                                    </Typography>
                                                    </Box>
                                                </Grid>
                                            </Grid>
                                        </Box>
                                    ))}

                            {/* if image result found show it */}
                            {stepsImages[step] && (
                                <Paper >
                                    <img
                                        src={base64_src(stepsImages[step])}
                                        width="100%"
                                        alt={`Step ${step}`}
                                        className={classes.paper_image}
                                        id={`step-${step}-result-image`}
                                    />
                                </Paper>
                            )}

                            {/* if result content got from server show it */}
                            {stepContents[step].result && (
                                <Panel
                                    variant={
                                        stepContents[step].bad
                                            ? "error"
                                            : "success"
                                    }
                                >
                                    <span id={`step-${step}-result-text`}>{stepContents[step].result}</span>
                                </Panel>
                            )}

                            {/* if error found show it */}
                            {errorSteps[step] && (
                                <Panel variant="error">
                                    <span id={`step-${step}-error`}>{errorSteps[step]}</span>
                                </Panel>
                            )}
                        </React.Fragment>
                    );
                }
            default:
                return "Unknown Execution step";
        }
    }

    return (
        <React.Fragment>
            <NunetHeading icon={ExecuteIcon}>ExecuteTask</NunetHeading>

            <Stepper
                activeStep={activeStep}
                orientation="vertical"
                nonLinear
                className={classes.stepper}
            >
                {steps.map((label, index) => (
                    <Step key={index}>
                        <StepButton
                            onClick={handleStep(index)}
                            completed={completedSteps[index]}
                            disabled={disabledSteps[index]}
                            id={`step-${index}-button`}
                        >
                            <StepLabel
                                error={errorSteps[index]}
                                style={{ width: "100%" }}
                            >
                                {
                                    <Typography>
                                        <span id={`step-${index}-icon`}>{step_icons[index]}</span>
                                        <span id={`step-${index}-label`}>{label}</span>
                                        {index !== STEP.SELECT &&
                                            (completedSteps[index] ||
                                                errorSteps[index]) && (
                                                <InfoIcon id={`step-${index}-info-icon`}
                                                    className={
                                                        classes.step_info_icon
                                                    }
                                                />
                                            )}
                                    </Typography>
                                }
                                {onwork[index] && (
                                    <CircularProgress size={20} id={`step-${index}-circular-progress`} />
                                )}
                            </StepLabel>
                        </StepButton>
                        <StepContent id={`step-${index}-content`}>{getStepJSX(index)}</StepContent>
                    </Step>
                ))}
            </Stepper>

            {result_id && !onwork[STEP.CNTK] && (
                <Box mb={4}>
                    <Button
                        variant="contained"
                        color="primary"
                        id="overall-result-button"
                        startIcon={<ResultIcon id="overall-result-icon" />}
                        onClick={goto_result(result_id, null, "last-run")}
                    >
                        <span id="overall-result-text">overall result</span>
                    </Button>
                </Box>
            )}
            <Box mt={5}>
                <Copyright />
            </Box>
        </React.Fragment>
    );
}

