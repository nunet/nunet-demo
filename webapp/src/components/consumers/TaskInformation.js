import React, { useState, useEffect } from "react";
import { Link as RouterLink } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import {Helmet} from 'react-helmet';

import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import Container from "@material-ui/core/Container";
import CardMedia from "@material-ui/core/CardMedia";
import CircularProgress from "@material-ui/core/CircularProgress";
import Box from "@material-ui/core/Box";
import Link from "@material-ui/core/Link";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";


import ExplanationIcon from "mdi-material-ui/Compass";
import OutputIcon from "mdi-material-ui/WeatherCloudyArrowRight";
import ResourceIcon from "@material-ui/icons/Toys";
import RewardIcon from "mdi-material-ui/GiftOutline";
import NunetIcon from "../../icons/IconsNunet10";
import DogIcon from "@material-ui/icons/Pets";
import StartTaskButton from "../commons/StartTask";
import IconTypography from "../commons/IconTypography";
import { UserContext } from "../utils/contexts";
import HelpIcon from '@material-ui/icons/Help';

import Copyright from "../commons/Copyright";

const useStyles = makeStyles(theme => ({
    gutterTop: {
        paddingTop: theme.spacing(2)
    },
    innerContainer: {
        padding: 0
    },
    half_divider: {
        width: "40%",
        margin: "0 auto"
    },
    under_nunet_icon: {
        marginTop: theme.spacing(-3)
    },
    nunet_icon: {
        height: "8rem",
        marginTop: "0rem"
    }
}));

function createData(picture, breed, base64, time, discovered_times) {
    return {
        picture: base64 ? base64 : "breeds/" + picture,
        breed: breed,
        time: time,
        discovered_times: discovered_times + " times"
    };
}

const sampleOutput = createData(
    "golden_retriever.jpeg",
    "Golden Retriever",
    null,
    "1 minute ago",
    6
);

export default function TaskInformation(props) {
    const classes = useStyles();

    const [loading, setLoading] = useState(true);
    const [isFirstLogIn, setFirstLogin] = useState(false);
    const [userInfo, setUserInfo] = React.useContext(UserContext);

    const paymentPerTask = 15;

    useEffect(() => {
        if (userInfo === false) {
            setLoading(false);
        } else if (userInfo) {
            setLoading(false);
        }
    }, [userInfo]);

    function goto_reward_table() {
        props.history.push("/rewards");
    }

    return (
        <React.Fragment>
            {loading ? (
                <CircularProgress />
            ) : (
                <React.Fragment>
                    {/**
                     *  Show the initial token award if the user is
                     *  logging in for the first time
                     */
                    isFirstLogIn && (
                        <React.Fragment>
                            <Typography
                                variant="h5"
                                gutterBottom
                                className={classes.gutterTop}
                            >
                                You are awarded
                            </Typography>
                            <Typography variant="h5">
                                {userInfo.getBalance()}NTXd tokens!
                            </Typography>
                        </React.Fragment>
                    )}
                    
                    <Helmet>
                        <script 
                            type="text/javascript" 
                            src="//cdn.iubenda.com/cs/tcf/stub.js">
                        </script>
                        <script type="text/javascript">{`
                            var _iub = _iub || [];_iub.csConfiguration = 
                            {
                            'lang':'en',
                            "siteId":1654663, 
                            "enableCMP":true,
                            "consentOnScroll":false,
                            "gdprAppliesGlobally":false,
                            "cookiePolicyId":67148905,
                            "banner": {
                                "acceptButtonDisplay":true,
                                "customizeButtonDisplay":true,
                                "position":"float-top-center" 
                                }
                            };
                            `}
                        </script>
                        <script 
                            type="text/javascript" 
                            src="//cdn.iubenda.com/cs/iubenda_cs.js" 
                            charset="UTF-8" 
                            async>
                        </script>
                    </Helmet>

                    <NunetIcon className={classes.nunet_icon} />
                    {userInfo && (
                        <Typography
                            variant="h6"
                            gutterBottom
                            className={classes.under_nunet_icon}
                        >
                            Your current balance of {userInfo.getBalance()}NTXd{" "}
                            tokens allows you to run{" "}
                            {Math.floor(userInfo.getBalance() / paymentPerTask)}{" "}
                            computational tasks
                        </Typography>
                    )}
                </React.Fragment>
            )}

            <React.Fragment>
                <Container
                    className={classes.innerContainer}
                    style={{ marginTop: 20 }}
                    align="left"
                >
                    <IconTypography
                        variant="h6"
                        gutterBottom
                        icon={ExplanationIcon}
                    >
                        Computational task
                    </IconTypography>
                    <Divider></Divider>
                    <Typography
                        variant="body1"
                        className={classes.gutterTop}
                        gutterBottom
                        align="justify"
                    >
                        NuNet runs{" "}
                        <Link
                            href="https://beta.singularitynet.io/"
                            underline="always"
                            target="_blank"
                        >
                            SingularityNET
                        </Link>{" "}
                        AI services by efficiently combining computing power
                        from multiple independent providers seamlessly. These
                        providers are rewarded with NTXd
                        <sup>
                            <Link href="#note1">1</Link>
                        </sup>{" "}
                        tokens in return.
                        <br />
                        <br />
                        This demo allows you upload a image of dog which the AI
                        service will determine the dog type. In return you will
                        be rewarded a bounty of NTXd
                        <sup>
                            <Link href="#note1">1</Link>
                        </sup>{" "}
                        tokens based on the breed of the dog type detected.
                    </Typography>

                    <Divider></Divider>

                    <object
                        style={{ height: "auto", width: "100%" }}
                        type="image/svg+xml"
                        data={
                            "/architecture/nunet_demo_architecture-01_objects.svg"
                        }
                    >
                        {" "}
                    </object>

                    <IconTypography
                        variant="h6"
                        gutterBottom
                        icon={ResourceIcon}
                        className={classes.gutterTop}
                    >
                        Resource Requirements
                        <Link to={`/help#telemetry`} component={RouterLink}>
                            <HelpIcon />
                        </Link>
                    </IconTypography>
                    <Divider></Divider>

                    <Box textAlign="center" pt={2} clone>
                        <Grid container spacing={2}>
                            <Grid item sm={3} xs={6}>
                                <Typography variant="body1" gutterBottom>
                                    CPU
                                </Typography>
                                
                                <Divider className={classes.half_divider} />
                                <Box mt={1}>
                                    <Typography variant="body1" gutterBottom>
                                        85 Mticks
                                    </Typography>
                                </Box>
                            </Grid>
                            <Grid item sm={3} xs={6}>
                                <Typography variant="body1" gutterBottom>
                                    RAM
                                </Typography>
                                <Divider className={classes.half_divider} />
                                <Box mt={1}>
                                    <Typography variant="body1" gutterBottom>
                                        8596 MBs
                                    </Typography>
                                </Box>
                            </Grid>
                            <Grid item sm={3} xs={6}>
                                <Typography variant="body1" gutterBottom>
                                    NET
                                </Typography>
                                <Divider className={classes.half_divider} />
                                <Box mt={1}>
                                    <Typography variant="body1" gutterBottom>
                                        9 KB
                                    </Typography>
                                </Box>
                            </Grid>
                            <Grid item sm={3} xs={6}>
                                <Typography variant="body1" gutterBottom>
                                    TIME
                                </Typography>
                                <Divider className={classes.half_divider} />
                                <Box mt={1}>
                                    <Typography variant="body1" gutterBottom>
                                        25 s
                                    </Typography>
                                </Box>
                            </Grid>
                        </Grid>
                    </Box>

                    <IconTypography
                        variant="h6"
                        gutterBottom
                        icon={OutputIcon}
                        className={classes.gutterTop}
                    >
                        Sample Output
                    </IconTypography>
                    <Divider></Divider>

                    <Card>
                        <CardMedia component="img" src={sampleOutput.picture} />
                        <CardContent>
                            <Box px={2} align="center">
                                <IconTypography
                                    variant="h6"
                                    gutterBottom
                                    icon={DogIcon}
                                >
                                    {sampleOutput.breed}
                                </IconTypography>

                                <Typography
                                    variant="h6"
                                    color="textSecondary"
                                    gutterBottom
                                >
                                    99.2% confidence
                                </Typography>
                            </Box>
                        </CardContent>
                    </Card>
                </Container>

                <Grid
                    container
                    className={classes.gutterTop}
                    spacing={0}
                    justify="space-evenly"
                    alignItems="center"
                >
                    <Grid item xs={6}>
                        <Button
                            variant="contained"
                            color="primary"
                            startIcon={<RewardIcon />}
                            onClick={goto_reward_table}
                        >
                            Reward List
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <StartTaskButton history={props.history} />
                    </Grid>
                </Grid>
                <Grid>
                    <p id="note1">
                        NTXd<sup>1</sup> tokens are designated for the
                        demonstration purposes within the scope of this
                        application only. See&nbsp;
                        <Link component={RouterLink} to="/legalTerms">
                            Terms of service.
                        </Link>
                    </p>
                </Grid>
                <Box mt={5}>
                    <Copyright />
                </Box>
            </React.Fragment>
        </React.Fragment>
    );
}
