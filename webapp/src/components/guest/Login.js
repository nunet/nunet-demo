import React, { useState, useContext } from "react";
import { Link as RouterLink } from "react-router-dom";

import { makeStyles, useTheme } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import Checkbox from "@material-ui/core/Checkbox";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import cryptoRandomString from "crypto-random-string";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import FormControlLabel from '@material-ui/core/FormControlLabel';


import Copyright from "../commons/Copyright";
import { call_grpc, set_local_token } from "../utils/grpc";
import { SnackBarContext, CacheContext } from "../utils/contexts";
import get_device_info from "../utils/devices";

import Facebook from "./facebook";
import Google from "./google";

const { LoginInput } = require("../../grpc/session_pb");

const useStyles = makeStyles(theme => ({
    main: {
        paddingTop: theme.spacing(4)
    },
    paper: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center"
    },
    logo: {
        padding: theme.spacing(2),
        marginBottom: theme.spacing(1),
        width: "100%"
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        padding: theme.spacing(0, 2)
    },
    card_header: {
        paddingBottom: 0
    },
    card_content: {
        paddingBottom: theme.spacing(1),
        paddingTop: theme.spacing(1)
    },
    card: {
        border: "1px solid rgba(255, 255, 255, 0.12)"
    }
}));

const styles = {
    card_content: {
        paddingBottom: 16,
        paddingTop: 8
    }
};

export default function SignIn(props) {
    const classes = useStyles();
    const theme = useTheme();
    const showSnackBar = useContext(SnackBarContext);
    const [cache, updateCache] = useContext(CacheContext);
    const [guestAgree, setGuestAgree] = useState(false);
    const [mediaAgree, setMediaAgree] = useState(false);

    const nunet_logo =
        theme.palette.type === "dark"
            ? "/logos/Nunet-text-dark-background.png"
            : "/logos/Nunet-text-light-background.png";

    const [emailName, setEmailName] = useState(""); // canbe username or email
    const [password, setPassword] = useState("");

    const changeUsing = (valueSetter, validator = value => { }) => event => {
        const { type, checked, name } = event.target;
        if (type === "checkbox" && name === "guestCheckbox") {
            valueSetter(checked);
            setGuestAgree(checked);
        } else if (type === "checkbox" && name === "mediaCheckbox") {
            valueSetter(checked);
            setMediaAgree(checked);
        } else {
            let value = event.target.value.trim();
            valueSetter(value);
            validator(value);
        }
    };

    function login() {
        if (!emailName) {
            showSnackBar("Email can not be empty.");
            return;
        }
        if (!password) {
            showSnackBar("Password can not be empty.");
            return;
        }

        let request = new LoginInput();
        request.setEmail(emailName);
        request.setPassword(password);
        request.setDeviceName(get_device_info());

        call_grpc("login", request, handle_login_response, showSnackBar);
    }

    function handle_login_response(response) {
        let token = response.getAccessToken();
        let firstLogin = response.getFirstLogin();

        if (token) {
            set_local_token(token);
            updateCache({}, true); // clear cache

            // remove if error snackbar was shown
            showSnackBar("");

            // go to start page
            if (firstLogin) {
                props.history.push("/info");
            } else {
                props.history.push("/consumer");
            }
        } else {
            showSnackBar("Server not responding. Please, try again.");
        }
    }

    function guest_login() {
        let request = new LoginInput();
        request.setEmail(cryptoRandomString({ length: 32, type: "url-safe" }));
        request.setPassword(
            cryptoRandomString({ length: 32, type: "url-safe" })
        );
        request.setDeviceName(get_device_info());

        call_grpc("guestLogin", request, handle_login_response, showSnackBar);
    }

    function onKeyPress(event) {
        if (event.key === "Enter") {
            login();
        }
    }

    return (
        <Container component="main" maxWidth="xs" className={classes.main}>
            <div className={classes.paper}>
                <img src={nunet_logo} alt="Logo" className={classes.logo} />
                <form className={classes.form} noValidate>
                    <Box py={1}>
                        <Card>
                            <CardHeader
                                className={classes.card_header}
                                title={
                                    <Box align="center">
                                        <Typography>Login as Guest</Typography>
                                    </Box>
                                }
                            />
                            <CardContent style={styles.card_content}>
                                <Box
                                    bgcolor="common.white"
                                    color="secondary.main"
                                    my={1}
                                    clone
                                >
                                    <Button
                                        type="button"
                                        fullWidth
                                        variant="contained"
                                        onClick={guest_login}
                                        disabled={Boolean(!guestAgree)}
                                    >
                                        Guest Login
                                    </Button>
                                </Box>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            value="allowExtraEmails"
                                            color="primary"
                                            name="guestCheckbox"
                                            checked={guestAgree}
                                            onChange={changeUsing(setGuestAgree)}
                                        />
                                    }
                                    label={
                                        <Typography variant="caption">
                                            I agree with
                                        <span>  </span>
                                            <Link
                                                to="/legalTerms"
                                                underline="hover"
                                                component={RouterLink}
                                            >
                                                Terms of Service
                                        </Link>
                                            <span> & </span>
                                            <Link
                                                to="/legalTerms/privacyPolicy"
                                                title="Privacy Policy "
                                                component={RouterLink}
                                            >
                                                Privacy Policy
                                        </Link>
                                        </Typography>
                                    }
                                >
                                </FormControlLabel>

                            </CardContent>
                        </Card>
                    </Box>
                    <Box py={1}>
                        <Card raised={true} elevation={5}>
                            <CardHeader
                                className={classes.card_header}
                                title={
                                    <Box align="center">
                                        <Typography>
                                            Login with Social Media
                                    </Typography>
                                    </Box>
                                }
                            />
                            <CardContent style={styles.card_content}>
                                {/* <Box py={1}>
                                <Divider />
                            </Box> */}
                                <Google history={props.history} mediaDisabled={!mediaAgree} />
                                <Facebook history={props.history} mediaDisabled={!mediaAgree} />
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            value="allowExtraEmails"
                                            color="primary"
                                            name="mediaCheckbox"
                                            checked={mediaAgree}
                                            onChange={changeUsing(setMediaAgree)}
                                        />
                                    }
                                    label={
                                        <Typography variant="caption">
                                            I agree with
                                        <span>  </span>
                                            <Link
                                                to="/legalTerms"
                                                underline="hover"
                                                component={RouterLink}
                                            >
                                                Terms of Service
                                        </Link>
                                            <span> & </span>
                                            <Link
                                                to="/legalTerms/privacyPolicy"
                                                title="Privacy Policy "
                                                component={RouterLink}
                                            >
                                                Privacy Policy
                                        </Link>
                                        </Typography>
                                    }
                                >
                                </FormControlLabel>
                            </CardContent>
                        </Card>
                    </Box>

                    <Box py={1}>
                        <Card>
                            <CardHeader
                                className={classes.card_header}
                                title={
                                    <Box align="center" pt={2}>
                                        <Typography>
                                            Login with Username or Email
                                        </Typography>
                                    </Box>
                                }
                            />
                            <CardContent style={styles.card_content}>
                                <TextField
                                    value={emailName}
                                    onChange={changeUsing(setEmailName)}
                                    onKeyPress={onKeyPress}
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Username or Email"
                                    name="email"
                                    autoComplete="email username"
                                    autoFocus
                                />
                                <TextField
                                    value={password}
                                    onChange={changeUsing(setPassword)}
                                    onKeyPress={onKeyPress}
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                />
                                <Box mt={3} mb={1}>
                                    <Button
                                        type="button"
                                        fullWidth
                                        variant="contained"
                                        color="primary"
                                        onClick={login}
                                    >
                                        Sign In
                                    </Button>
                                </Box>

                                {/* <Link href="#" variant="caption">
                        Forgot password?
                    </Link> */}

                                <Box pt={2}>
                                    <Typography align="center">
                                        Don't have an account?
                                        <br />
                                        <Link
                                            component={RouterLink}
                                            to="/signup"
                                            variant="body1"
                                        >
                                            Create Account
                                        </Link>
                                    </Typography>
                                </Box>
                            </CardContent>
                        </Card>
                    </Box>
                </form>
            </div>

            <Box mt={2}>
                <Copyright />
            </Box>
        </Container>
    );
}
