import React, { useState, useEffect, useContext } from "react";
import { Link as RouterLink } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";

import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import CardActions from "@material-ui/core/CardActions";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Box from "@material-ui/core/Box";

import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ResourcesIcon from "@material-ui/icons/Toys";
import TaskIcon from "@material-ui/icons/Assignment";
import CoinIcon from "mdi-material-ui/Coins";
import JoinIcon from "@material-ui/icons/GroupAdd";
import TryDemoIcon from "mdi-material-ui/Ungroup";

import { base64_src } from "../utils/images";
import { call_grpc } from "../utils/grpc";
import IconTypography from "../commons/IconTypography";
import Loading from "../commons/Loading";
import { SnackBarContext } from "../utils/contexts";
import ShowInfo from "../commons/ShowInfo";

const { ProcessInfoInput } = require("../../grpc/session_pb");

const useStyles = makeStyles(theme => ({
    lessGutterTop: {
        paddingTop: theme.spacing(1)
    },
    section_header: {
        fontSize: "1rem",
        fontWeight: theme.typography.fontWeightMedium
    },
    json_details: {
        padding: 0,
        overflowX: "auto" // make scrollabe json content get out of control
    },
    half_divider: {
        width: "40%",
        margin: "0 auto"
    },
    breed_name: {
        textTransform: "capitalize"
    }
}));

export default function ShareTask(props) {
    const classes = useStyles();

    const showSnackBar = useContext(SnackBarContext);

    const [loading, setLoading] = useState(false);

    const [task, setTask] = useState(null);
    const [extractedResult, setExtractedResult] = useState(null);
    const [jsonResult, setJsonResult] = useState(null);

    // get information from url
    const query = new URLSearchParams(props.location.search);
    const share_token = query.get("id");

    function on_grpc_error(err) {
        setLoading(false);
    }

    function get_task_result() {
        if (share_token === null) {
            showSnackBar('Invalid sharing link. Please, get the sharing link and try again.');
            return;
        }

        setLoading(true);
        let request = new ProcessInfoInput();
        request.setShareToken(share_token);

        call_grpc(
            "processInfo",
            request,
            on_task_result_reponse,
            showSnackBar,
            on_grpc_error
        );
    }

    function on_task_result_reponse(response) {
        setLoading(false);
        setTask(response);

        let result = response.getResult();
        let top_5 = response.getJsonResult();

        let extract = extract_from_result(result);
        if (extract) {
            setExtractedResult(extract);
            setJsonResult({
                BREED: extract.breed,
                CONFIDENCE: extract.confidence,
                TOP_5: top_5 || "No top 5 data found"
            });
        }
    }

    /**
     * Extracts breed and confidence from backend result string (beware very breakable code)
     * @param {str} result : 'Dog breed is breed_name with confindence_number confidence' format string
     */
    function extract_from_result(result) {
        let start = "Dog breed is ";
        if (result.startsWith(start)) {
            result = result.slice(start.length);
            let words = result.split(" ");

            let breed, confidence;
            if (words[0]) {
                // first element of split is breed name
                breed = words[0];
                breed = breed.replace("_", " ");
            } else {
                throw Error(
                    `Breed could not be found in result string: ${result}`
                );
            }

            if (words[2]) {
                // third element of split is confidence number value
                confidence = words[2];
            } else {
                throw Error(
                    `Confidence could not be found in result string: ${result}`
                );
            }
            return { breed: breed, confidence: confidence };
        } else {
            return null;
        }
    }

    useEffect(() => {
        if (!task) {
            get_task_result();
        }
    });

    return (
        <React.Fragment>
            {loading ? (
                <Loading />
            ) : (
                task && (
                    <React.Fragment>
                        <IconTypography
                            variant="h4"
                            icon={TaskIcon}
                            mb={2}
                            mt={1}
                        >
                            Detected dog breed in an image
                        </IconTypography>

                        <Card elevation={2}>
                            {task.getImageOutput() && (
                                <CardMedia
                                    component="img"
                                    src={base64_src(task.getImageOutput())}
                                    title={`Executed Task result`}
                                />
                            )}

                            <CardContent>
                                {extractedResult ? (
                                    <React.Fragment>
                                        <Typography
                                            gutterBottom
                                            variant="h5"
                                            component="h5"
                                            className={classes.breed_name}
                                        >
                                            {extractedResult.breed}
                                        </Typography>
                                        <Typography
                                            variant="body2"
                                            color="textSecondary"
                                            component="p"
                                        >
                                            {extractedResult.confidence}{" "}
                                            confidence
                                        </Typography>
                                    </React.Fragment>
                                ) : (
                                    <Typography
                                        gutterBottom
                                        variant="h5"
                                        component="h5"
                                    >
                                        {task.getResult()}
                                    </Typography>
                                )}
                            </CardContent>
                            <CardActions
                                style={{ justifyContent: "center" }}
                            ></CardActions>
                        </Card>

                        <ExpansionPanel elevation={2}>
                            <ExpansionPanelSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <Typography
                                    variant="h6"
                                    className={classes.section_header}
                                >
                                    JSON
                                </Typography>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails
                                className={classes.json_details}
                            >
                                {jsonResult ? (
                                    <Table>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>
                                                    <b>Key</b>
                                                </TableCell>
                                                <TableCell>
                                                    <b>Value</b>
                                                </TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {Object.keys(jsonResult).map(
                                                key => (
                                                    <TableRow key={key}>
                                                        <TableCell
                                                            component="th"
                                                            scope="row"
                                                        >
                                                            {key}
                                                        </TableCell>
                                                        <TableCell>
                                                            {jsonResult[key]}
                                                        </TableCell>
                                                    </TableRow>
                                                )
                                            )}
                                        </TableBody>
                                    </Table>
                                ) : (
                                    <ShowInfo flex="auto">
                                        No json data found.
                                    </ShowInfo>
                                )}
                            </ExpansionPanelDetails>
                        </ExpansionPanel>

                        <Box textAlign="center" clone>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <Box mt={4} textAlign="center" clone>
                                        <IconTypography
                                            variant="h6"
                                            icon={ResourcesIcon}
                                            style={{
                                                textTransform: "uppercase"
                                            }}
                                        >
                                            USED DECENTRALIZED COMPUTE RESOURCES
                                        </IconTypography>
                                    </Box>
                                </Grid>

                                <Grid item sm={4} xs={6}>
                                    <Typography variant="body1" gutterBottom>
                                        Total CPU
                                    </Typography>
                                    <Divider className={classes.half_divider} />
                                    <Typography
                                        variant="body1"
                                        gutterBottom
                                        className={classes.lessGutterTop}
                                    >
                                        {task.getCpuUsage().toFixed(2)}
                                        Mticks
                                    </Typography>
                                </Grid>
                                <Grid item sm={4} xs={6}>
                                    <Typography variant="body1" gutterBottom>
                                        RAM
                                    </Typography>
                                    <Divider className={classes.half_divider} />
                                    <Typography
                                        variant="body1"
                                        gutterBottom
                                        className={classes.lessGutterTop}
                                    >
                                        {task.getTotalMemory().toFixed(2)}
                                        MBs
                                    </Typography>
                                </Grid>
                                <Grid item sm={4} xs={6}>
                                    <Typography variant="body1" gutterBottom>
                                        Max RAM
                                    </Typography>
                                    <Divider className={classes.half_divider} />
                                    <Typography
                                        variant="body1"
                                        gutterBottom
                                        className={classes.lessGutterTop}
                                    >
                                        {task.getMemoryUsage().toFixed(2)}
                                        MB
                                    </Typography>
                                </Grid>

                                <Grid item sm={4} xs={6}>
                                    <Typography variant="body1" gutterBottom>
                                        TIME
                                    </Typography>
                                    <Divider className={classes.half_divider} />
                                    <Typography
                                        variant="body1"
                                        gutterBottom
                                        className={classes.lessGutterTop}
                                    >
                                        {task.getTimeTaken().toFixed(2)}s
                                    </Typography>
                                </Grid>
                                <Grid item sm={4} xs={6}>
                                    <Typography variant="body1" gutterBottom>
                                        NET
                                    </Typography>
                                    <Divider className={classes.half_divider} />
                                    <Typography
                                        variant="body1"
                                        gutterBottom
                                        className={classes.lessGutterTop}
                                    >
                                        {task.getNetworkRx().toFixed(2)}KB
                                    </Typography>
                                </Grid>
                                <Grid item sm={4} xs={6}>
                                    <Typography variant="body1" gutterBottom>
                                        NET Time
                                    </Typography>
                                    <Divider className={classes.half_divider} />
                                    <Typography
                                        variant="body1"
                                        gutterBottom
                                        className={classes.lessGutterTop}
                                    >
                                        {(
                                            task.getNetworkRx() /
                                            task.getTimeTaken()
                                        ).toFixed(2)}
                                        KB/s
                                    </Typography>
                                </Grid>

                                <Grid item xs={12}>
                                    <Box mt={2} textAlign="center" clone>
                                        <IconTypography
                                            variant="h6"
                                            icon={CoinIcon}
                                            style={{
                                                textTransform: "uppercase"
                                            }}
                                        >
                                            EARNED NUNET TOKENS
                                        </IconTypography>
                                    </Box>
                                </Grid>
                                <Grid item xs={4}>
                                    <Typography variant="body1" gutterBottom>
                                        {task.getTokenReward()}
                                        NTXd
                                    </Typography>
                                    <Typography variant="body1" gutterBottom>
                                        REWARD
                                    </Typography>
                                </Grid>

                                <Grid item xs={4}>
                                    <Typography variant="body1" gutterBottom>
                                        {task.getTokenSpent()}NTXd
                                    </Typography>
                                    <Typography variant="body1" gutterBottom>
                                        COMPUTING COSTS
                                    </Typography>
                                </Grid>
                                <Grid item xs={4}>
                                    <Typography variant="body1" gutterBottom>
                                        {task.getTokenReward() -
                                            task.getTokenSpent()}
                                        NTXd
                                    </Typography>
                                    <Typography variant="body1" gutterBottom>
                                        EARNED
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <Divider />
                                </Grid>
                                <Grid item xs={6} mt={10}>
                                    <Typography variant="body1" gutterBottom>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            startIcon={<TryDemoIcon />}
                                            component="a"
                                            href="https://demo.nunet.io"
                                            target="_blank"
                                        >
                                            Try demo
                                        </Button>
                                    </Typography>
                                </Grid>
                                <Grid item xs={6}>
                                    <Typography variant="body1" gutterBottom>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            startIcon={<JoinIcon />}
                                            component="a"
                                            href="https://nunet.io"
                                            target="_blank"
                                        >
                                            JOIN NUNET
                                        </Button>
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Box>
                    </React.Fragment>
                )
            )}
        </React.Fragment>
    );
}
