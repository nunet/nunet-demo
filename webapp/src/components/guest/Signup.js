import React, { useState, useContext } from "react";
import { Link as RouterLink } from "react-router-dom";

import { makeStyles, useTheme } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import FormControlLabel from '@material-ui/core/FormControlLabel';

import WarningIcon from "@material-ui/icons/Warning";
import { call_grpc } from "../utils/grpc";
import Copyright from "../commons/Copyright";
import { SnackBarContext } from "../utils/contexts";
import get_device_info from "../utils/devices";

const grpc = require("grpc-web");

const { SignupInput } = require("../../grpc/session_pb");

const useStyles = makeStyles(theme => ({
    main: {
        paddingTop: theme.spacing(4)
    },
    paper: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center"
    },
    logo: {
        padding: theme.spacing(2),
        marginBottom: theme.spacing(1),
        width: "100%"
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        padding: theme.spacing(0, 2)
    }
}));

export default function Signup(props) {
    const classes = useStyles();
    const theme = useTheme();
    const showSnackBar = useContext(SnackBarContext);
    const nunet_logo =
        theme.palette.type === "dark"
            ? "/logos/Nunet-text-dark-background.png"
            : "/logos/Nunet-text-light-background.png";

    const [emailName, setEmailName] = useState("");
    const [password, setPassword] = useState("");
    const [passwordError, setPasswordError] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [confirmError, setConfirmError] = useState("");
    const [emailNameError, setEmailNameError] = useState("");
    const [agree, setAgree] = useState(false);

    const signupDisabled =
        emailNameError ||
        passwordError ||
        confirmError ||
        !agree ||
        !emailName ||
        !password ||
        !confirmPassword;

    const changeUsing = (valueSetter, validator) => event => {
        const { type, checked } = event.target;
        if (type === "checkbox") {
            setAgree(checked);
        } else {
            let value = event.target.value.trim();
            valueSetter(value);
            validator(value);
        }
    };

    function validatePassword(passwordValue) {
        //? remove checking for strong password
        // let mediumRegex = new RegExp(
        //     "^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})"
        // );
        if (passwordValue.length < 4) {
            setPasswordError("Password length must be at least 4 characters.");
            //? remove checking for strong password
            // } else if (!mediumRegex.test(passwordValue)) {
            //     setPasswordError(
            //         "Password must contain one of capitals, numbers or punctuation."
            //     );
        } else {
            setPasswordError("");
        }

        // validate confirm password using new password value
        confirmPassword && validateConfirmPassword(null, passwordValue);
    }

    function validateConfirmPassword(confirmPasswordValue, newPassword = null) {
        if (newPassword === null) {
            if (password === confirmPasswordValue) {
                setConfirmError("");
            } else {
                setConfirmError("Password didnot match.");
            }
        } else if (newPassword === confirmPassword) {
            setConfirmError("");
        } else {
            setConfirmError("Password didnot match.");
        }
    }

    function validateEmail(address) {
        let re = /\S+@\S+\.\S+/;
        if (address.match(re)) {
            setEmailNameError("");
        } else {
            setEmailNameError("Your email address is not valid.");
        }
    }

    // validation for username or email
    function validateEmailName(emailName) {
        let onlyAt = /^\w+@\w+$/;
        let onlyDot = /^\w+\.\w+$/;
        let emailRegex = /^\w+@\w+\.\w+$/;
        let usernameRegex = /^\w+$/;
        if (!usernameRegex.test(emailName) && !emailRegex.test(emailName)) {
            if (onlyAt.test(emailName) || onlyDot.test(emailName)) {
                setEmailNameError("Your email address is not valid.");
            } else {
                setEmailNameError(
                    "Username cannot contain special characters."
                );
            }
        } else {
            setEmailNameError("");
        }
    }

    function signup_response_handler(response) {
        const status = response.getStatus();

        if (status === grpc.StatusCode.OK) {
            showSnackBar("Successfully registered user.", "success");
            props.history.push("/login");
        }
    }

    function signup() {
        // subtle validation of username or email field
        if (emailName.length <= 1) {
            showSnackBar("Username should be greater than 1 character.");
            return;
        }

        let request = new SignupInput();
        request.setEmail(emailName);
        request.setPassword(password);
        request.setDeviceName(get_device_info());

        call_grpc("signup", request, signup_response_handler, showSnackBar);
    }

    function ErrorMessage(props) {
        return (
            <Box color="error.main" display="flex">
                <Box pr={1.4}>
                    <WarningIcon />
                </Box>
                {props.children}
            </Box>
        );
    }

    return (
        <Container component="main" maxWidth="xs" className={classes.main}>
            <div className={classes.paper}>
                <img src={nunet_logo} alt="Logo" className={classes.logo} />
                <Typography component="h1" variant="h5">
                    Sign up
                </Typography>
                <form className={classes.form} noValidate>
                    <TextField
                        value={emailName}
                        onChange={changeUsing(setEmailName, validateEmailName)}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Username or Email"
                        name="email"
                        autoComplete="email username"
                        autoFocus
                    />

                    {emailNameError && (
                        <ErrorMessage>{emailNameError}</ErrorMessage>
                    )}

                    <TextField
                        value={password}
                        onChange={changeUsing(setPassword, validatePassword)}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete=""
                    />

                    {passwordError && (
                        <ErrorMessage>{passwordError}</ErrorMessage>
                    )}

                    <TextField
                        value={confirmPassword}
                        onChange={changeUsing(
                            setConfirmPassword,
                            validateConfirmPassword
                        )}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="confirmPassword"
                        label="Confirm Password"
                        type="password"
                        id="confrimPassword"
                        autoComplete=""
                    />
                    {confirmError && (
                        <ErrorMessage>{confirmError}</ErrorMessage>
                    )}

                    <FormControlLabel 
                            control={
                                <Checkbox
                                value="allowExtraEmails"
                                label="Start"
                                label
                                color="primary"
                                name="agree"
                                checked={agree}
                                onChange={changeUsing(setAgree)}
                            />
                            }
                            label={
                                <Typography variant="caption">
                                I agree with
                                <span>  </span>
                                <Link
                                    to="/legalTerms"
                                    underline="hover"
                                    component={RouterLink}
                                >
                                    Terms of Service
                                </Link>
                                <span> & </span>
                                <Link
                                    to="/legalTerms/privacyPolicy" 
                                    title="Privacy Policy "
                                    component={RouterLink}
                                    > 
                                Privacy Policy
                                </Link>    
                                </Typography>
                                }
                        >
                    </FormControlLabel>

                    <Box mt={3} mb={1}>
                        <Button
                            onClick={signup}
                            fullWidth
                            variant="contained"
                            color="primary"
                            disabled={Boolean(signupDisabled)}
                        >
                            Sign Up
                        </Button>
                    </Box>

                    <Box pt={4}>
                        <Typography align="center">
                            Already have an account?
                            <br />
                            <Link
                                component={RouterLink}
                                to="/login"
                                variant="body1"
                            >
                                Sign in
                            </Link>
                        </Typography>
                    </Box>
                </form>
            </div>

            <Box mt={5}>
                <Copyright />
            </Box>
        </Container>
    );
}
