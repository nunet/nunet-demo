import React, { useContext, useState } from "react";

import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import GoogleIcon from "mdi-material-ui/Google";
import WarningIcon from "@material-ui/icons/Warning";
import GoogleLogin from "react-google-login";

import { SnackBarContext } from "../utils/contexts";
import { call_grpc, set_local_token } from "../utils/grpc";
import get_device_info from "../utils/devices";
const { SmediaLoginInput } = require("../../grpc/session_pb");

const useStyles = makeStyles(theme => ({
    media: {
        margin: theme.spacing(1, 0, 1),
        color: "#0A2042",
        backgroundColor: theme.palette.common.white
    }
}));

export default function Google(props) {
    const classes = useStyles();
    const showSnackBar = useContext(SnackBarContext);
    const [cookieError, setCookieError] = useState("");


    const responseGoogle = response => {
        let token = response.accessToken;
        let request = new SmediaLoginInput();
        request.setMedia("google");
        request.setAccessToken(token);
        request.setDeviceName(get_device_info());

        call_grpc(
            "socialMediaLogin",
            request,
            handle_social_login_response,
            showSnackBar
        );
    };

    const handleFailure = error => {
        let detail = error.details;
        if (detail === "Cookies are not enabled in current environment."){
      	    setCookieError("Please enable cookies or use another browser for signing in with Google");
        } else {
	    setCookieError("");
        }
        console.log("failure when google login:", error);
    };

    function handle_social_login_response(response) {
        let token = response.getAccessToken();
        let firstLogin = response.getFirstLogin();
        if (token) {
            set_local_token(token);
            if (firstLogin) {
                props.history.push("/info");
            } else {
                props.history.push("/consumer");
            }
        } else {
            showSnackBar("Server not responding. Please, try again.");
        }
    }
    
    function ErrorMessage(props){
        return(
            <Box color="error.main" display="flex">
                <Box pr={1.4}>
                    <WarningIcon />
                </Box>
                {props.children}
            </Box>
        );
    }
 
    return (
	<div>
        <GoogleLogin
            clientId={process.env.REACT_APP_GOOGLE_CLIENT}
            onSuccess={responseGoogle}
            onFailure={handleFailure}
            render={renderProps => (
                <Button
                    onClick={renderProps.onClick}
                    variant="contained"
                    fullWidth
                    className={classes.media}
                    startIcon={<GoogleIcon />}
                    disabled={Boolean(props.mediaDisabled)}
                >
                    GOOGLE
                </Button>
            )}
        />
	{cookieError && <ErrorMessage>{cookieError}</ErrorMessage>}
	</div>
    );
}
