import React, { useContext, useState, useEffect } from "react";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Box from "@material-ui/core/Box";

import DeviceIcon from "mdi-material-ui/DesktopTower";
import PriceIcon from "mdi-material-ui/Cash";

import NunetIcon from "../../icons/IconsNunet09";
import NunetHeading from "../commons/NunetHeading";
import IconTypography from "../commons/IconTypography";
import PaperTable from "../commons/PaperTable";
import Loading from "../commons/Loading";
import Copyright from "../commons/Copyright";
import { SnackBarContext } from "../utils/contexts";
import { call_grpc } from "../utils/grpc";

const { ProviderDeviceInput } = require("../../grpc/session_pb");

function DeviceDetails(props) {
    const showSnackBar = useContext(SnackBarContext);

    const [loading, setLoading] = useState(false);

    const [device, setDevice] = useState(null);

    // get information from parent page
    const device_name = props.match.params.device_name;

    function on_grpc_error(err) {
        setLoading(false);
    }

    function parseGrpcDevice(response) {
        return {
            cpu_limit: response.getCpuLimit().toFixed(2) + " Mticks/s",
            cpu_used: response.getCpuUsed().toFixed(2) + " Mticks",
            memory_limit: response.getMemoryLimit().toFixed(2) + " MB",
            memory_used: response.getMemoryUsed().toFixed(2) + " MBs",
            net_limit: response.getNetLimit().toFixed(2) + " KB/s",
            net_used: response.getNetUsed().toFixed(2) + " KB",
            max_up_time: response.getMaxUpTime().toFixed(2) + " %",
            used_up_time: response.getUsedUpTime().toFixed(2) + "s",
            cpu_price: response.getCpuPrice().toFixed(2) + " NTXd/Mtick",
            ram_price: response.getRamPrice().toFixed(3) + " NTXd/MBs",
            net_price: response.getNetPrice().toFixed(2) + " NTXd/KB"
        };
    }

    function get_device_info() {
        setLoading(true);
        let request = new ProviderDeviceInput();
        request.setDeviceName(device_name);

        call_grpc(
            "providerDevice",
            request,
            on_device_info,
            showSnackBar,
            on_grpc_error
        );
    }

    function on_device_info(response) {
        setLoading(false);
        setDevice(parseGrpcDevice(response));
    }

    useEffect(() => {
        if (!device) {
            get_device_info();
        }
    }, [device]);

    return (
        <React.Fragment>
            <NunetHeading icon={NunetIcon}>Device Details</NunetHeading>
            {loading ? (
                <Loading />
            ) : (
                device && (
                    <React.Fragment>
                        <IconTypography
                            gutterBottom
                            variant="h6"
                            icon={DeviceIcon}
                            mt={2}
                        >
                            {device_name}
                        </IconTypography>

                        <PaperTable>
                            <Table aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="center">
                                            <b>Resource</b>
                                        </TableCell>
                                        <TableCell align="center">
                                            <b>Dedicated</b>
                                        </TableCell>
                                        <TableCell align="center">
                                            <b>Used</b>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow>
                                        <TableCell align="center">
                                            CPU
                                        </TableCell>
                                        <TableCell align="center">
                                            {device.cpu_limit}
                                        </TableCell>
                                        <TableCell align="center">
                                            {device.cpu_used}
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell align="center">
                                            RAM
                                        </TableCell>
                                        <TableCell align="center">
                                            {device.memory_limit}
                                        </TableCell>
                                        <TableCell align="center">
                                            {device.memory_used}
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell align="center">
                                            Net
                                        </TableCell>
                                        <TableCell align="center">
                                            {device.net_limit}
                                        </TableCell>
                                        <TableCell align="center">
                                            {device.net_used}
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell align="center">
                                            Up time
                                        </TableCell>
                                        <TableCell align="center">
                                            {device.max_up_time}
                                        </TableCell>
                                        <TableCell align="center">
                                            {device.used_up_time}
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </PaperTable>

                        <IconTypography
                            gutterBottom
                            variant="h6"
                            icon={PriceIcon}
                            mt={2}
                        >
                            Price of resources
                        </IconTypography>

                        <PaperTable>
                            <Table aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="center">
                                            <b>Resource</b>
                                        </TableCell>
                                        <TableCell align="center">
                                            <b>Price</b>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow>
                                        <TableCell align="center">
                                            CPU
                                        </TableCell>
                                        <TableCell align="center">
                                            {device.cpu_price}
                                        </TableCell>
                                    </TableRow>

                                    <TableRow>
                                        <TableCell align="center">
                                            RAM
                                        </TableCell>
                                        <TableCell align="center">
                                            {device.ram_price}
                                        </TableCell>
                                    </TableRow>

                                    <TableRow>
                                        <TableCell align="center">
                                            Net
                                        </TableCell>
                                        <TableCell align="center">
                                            {device.net_price}
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </PaperTable>
                    </React.Fragment>
                )
            )}
            <Box mt={5}>
                <Copyright />
            </Box>
        </React.Fragment>
    );
}

export default DeviceDetails;
