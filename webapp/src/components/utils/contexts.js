import React from "react";

/**
 * A context to save userInfo so all components can get it whenever they need
 */
const UserContext = React.createContext(null);

/**
 * A context to hold application cache for all components
 * !! Beware, cache values are stored as an object with their name as key
 */
const CacheContext = React.createContext({});

/**
 * A context to hold shared global state as store (don't know if this approach is best but hate redux anyway)
 * !! Beware, store values are stored as an object with their name as key
 */
const StoreContext = React.createContext({});

/**
 * A context to get snackbar showing function for all components
 */
const SnackBarContext = React.createContext();

export { UserContext, CacheContext, StoreContext, SnackBarContext };
