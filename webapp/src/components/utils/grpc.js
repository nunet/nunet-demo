const grpc = require("grpc-web");
const { SessionManagerClient } = require("../../grpc/session_grpc_web_pb");

// grpc execute remote method response tag types
const TAGS = {
    log: "log",
    error: "error",
    yolo_stat: "yolo_stat",
    cntk_stat: "cntk_stat",
    next: "next",
    yolo_result: "yolo_result",
    cntk_result: "cntk_result",
    yolo_output: "yolo_output",
    bad_result: "bad_result",
    task_id: "task_id",
    on_queue:"on_queue"
};

/**
 * Get local access token of user
 * @returns saved access token string or undefined if not saved before
 */
function get_local_token() {
    return localStorage.getItem("access_token");
}

/**
 * Set access token locally for subsequent grpc calls
 * @param {str} token_value : token string found from grpc server
 */
function set_local_token(token_value) {
    localStorage.setItem("access_token", token_value);
}

/**
 * Checks if a user has logged in the app locally, no remote communication
 * @returns {str || null}: locally stored access token or null if not found
 */
function is_logged_in() {
    return get_local_token();
}

/**
 * Show grpc call error
 * @param {number} error_code : grpc error code
 * @param {str} error_message : grpc erro message if exists
 * @param {func} error_shower : error shower function
 */
function show_grpc_error(error_code, error_message, error_shower) {
    if (error_code === grpc.StatusCode.UNAVAILABLE) {
        // connection failure
        // ! also could be 400 and 500 level error from grpc(very rare)
        error_shower(
            "Could not connect to server. Please, check your connection."
        );
    } else if (error_code === grpc.StatusCode.UNKNOWN) {
        // uncatched exception from server implementation(most probably)
        if (error_message === "Exception iterating responses: NO_Sufficient_TOKEN"){
            error_shower("You have insufficient NTXd token balance to execute this task.");
        } else {
            error_shower("Server Error. Please, try again.");
        }
    } else if (error_code === grpc.StatusCode.UNAUTHENTICATED) {
        // user is not logged in or its credential is outdated
        localStorage.clear();
        error_shower("You have been logged out. Please, login again.");
    } else if (error_message) {
        // custom server exception with message from grpc implementation
        // ! also could be grpc errors which are not custom but have message attributes
        error_shower(error_message);
        console.log("grpc error with message attribute: ", error_message);
    }
}

/**
 * Create reusable grpc client with appropriate host
 */
let client = new SessionManagerClient(process.env.REACT_APP_GRPC_HOST);

/**
 * Call remote grpc method appropriately
 * @param {grpc_request} request grpc generated request data struture of the call
 * @param {str} method_name :grpc remote method name
 * @param {func} response_handler : handles grpc response
 * @param {func} showSnackBar : shows grpc error
 * @param {func} when_error: error handler function by caller
 */
function call_grpc(
    method_name,
    request,
    response_handler,
    showSnackBar,
    when_error
) {
    // attach access token if found
    let metadata = {};
    let access_token = get_local_token();
    if (access_token) {
        metadata = { access_token: access_token };
    }

    let stream = client[method_name](request, metadata, (err, response) => {
        console.log(
            `--------------------------in [${method_name}]grpc callback start--------------------`
        );
        console.log("grpc callback(err): ", err);
        console.log("grpc callback(response): ", response);
        // err and response both are returned mutually exclusively(weird)
        if (err) {
            show_grpc_error(err.code, err.message, showSnackBar);
            when_error && when_error(err);
        }

        if (response) {
            response_handler(response);
        } else {
            // when there is error, empty response is sent (weird)
            console.warn("empty response from server");
        }

        console.log(
            `--------------------------in [${method_name}]grpc callback end--------------------`
        );
    });

    /** think about below approach-- no I won't */
    // stream.on("data", function(response) {
    //     console.log("on data: ", response);
    //     console.log("log: ", response.getAccessToken());
    // });
    // stream.on("status", function(status) {
    //     if (status.code !== grpc.StatusCode.OK) {
    //         console.log('status: err', status.details);
    //     }
    //     console.log("status: ", status.code);
    //     console.log("status: ", status.details);
    //     console.log("status: ", status.metadata);
    // });
    // stream.on("end", function(end) {
    //     // stream end signal
    //     console.log("stream: end");
    // });
}

export {
    call_grpc,
    TAGS,
    show_grpc_error,
    client,
    get_local_token,
    set_local_token,
    is_logged_in
};
