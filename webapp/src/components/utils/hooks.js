import React from "react";

const useStateWithLocalStorage = (localStorageKey, defaultValue = null) => {
    let cache = localStorage.getItem(localStorageKey);
    let cacheValue = null;
    if (cache) {
        try {
            cacheValue = JSON.parse(cache);
        } catch (err) {
            localStorage.removeItem(localStorageKey);
        }
    }
    const [value, setValue] = React.useState(cacheValue || defaultValue);

    React.useEffect(() => {
        localStorage.setItem(localStorageKey, JSON.stringify(value));
    }, [value]);

    return [value, setValue];
};

export { useStateWithLocalStorage };
