// all page urls(routes)
//!! only task_result page is used with the whole app, maybe fix all others when have time or a specific route is used many times
const routes = {
    task_result: "/tasks/result",
    task_history: "/tasks",
    shared: "/shared",
};

export default routes;
