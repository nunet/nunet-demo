import React from "react";

const SvgIconsnunet07 = props => (
  <svg data-name="Layer 1" viewBox="0 0 600 600" {...props}>
    <defs>
      <linearGradient
        id="icons_nunet-07_svg__a"
        x1={108.93}
        y1={56.53}
        x2={425.37}
        y2={439.14}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#2e9ad6" />
        <stop offset={0.47} stopColor="#227ec0" />
        <stop offset={1} stopColor="#1259a3" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-07_svg__b"
        x1={108.59}
        y1={56.81}
        x2={425.03}
        y2={439.42}
        xlinkHref="#icons_nunet-07_svg__a"
      />
    </defs>
    <path
      d="M384.86 85.46h-191a93.65 93.65 0 00-93.65 93.65v191.12a93.65 93.65 0 0093.65 93.65h191a93.65 93.65 0 0093.66-93.65V179.11a93.65 93.65 0 00-93.66-93.65zm32.63 254.23a63.72 63.72 0 01-63.72 63.72h-130a63.72 63.72 0 01-63.72-63.72v-130a63.72 63.72 0 0163.72-63.72h130a63.72 63.72 0 0163.72 63.72z"
      fill="url(#icons_nunet-07_svg__a)"
    />
    <rect
      x={202.24}
      y={188.12}
      width={173.07}
      height={173.1}
      rx={42.84}
      fill="url(#icons_nunet-07_svg__b)"
    />
  </svg>
);

export default SvgIconsnunet07;
