import React from "react";

const SvgIconsnunet09 = props => (
  <svg data-name="Layer 1" viewBox="0 0 600 600" {...props}>
    <defs>
      <linearGradient
        id="icons_nunet-09_svg__a"
        x1={14.69}
        y1={43.97}
        x2={750.04}
        y2={663.8}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#2e9ad6" />
        <stop offset={0.47} stopColor="#227ec0" />
        <stop offset={1} stopColor="#1259a3" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-09_svg__b"
        x1={14.44}
        y1={44.26}
        x2={394.37}
        y2={364.5}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#6fc9d6" />
        <stop offset={1} stopColor="#1784b5" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-09_svg__c"
        x1={245.4}
        y1={240.28}
        x2={322.43}
        y2={305.21}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#00a79d" />
        <stop offset={0.53} stopColor="#17a1b9" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
    </defs>
    <path
      d="M379.11 81.42h-191a93.65 93.65 0 00-93.65 93.65v191.11a93.65 93.65 0 0093.65 93.66h191a93.66 93.66 0 0093.66-93.66V175.07a93.65 93.65 0 00-93.66-93.65zm32.63 254.22A63.72 63.72 0 01348 399.37H218a63.72 63.72 0 01-63.72-63.73v-130A63.72 63.72 0 01218 141.89h130a63.72 63.72 0 0163.72 63.72z"
      fill="url(#icons_nunet-09_svg__a)"
    />
    <path
      d="M363.74 194.34l-53.68-31.44c-14.44-8.53-38.17-8.58-52.74-.34l-54.14 30.74c-14.46 8.32-26.49 28.81-26.61 45.57L176.2 301c-.13 16.77 11.59 37.4 26.08 45.92L256 378.4c14.47 8.42 38.22 8.57 52.79.33l54-30.82c14.57-8.24 26.55-28.72 26.68-45.49l.39-62.27c.07-16.75-11.65-37.39-26.12-45.81zm-8.36 101c-.09 11.33-8.19 25.17-18 30.75l-36.52 20.83c-9.85 5.57-25.91 5.47-35.69-.22l-36.28-21.26c-9.79-5.75-17.72-19.7-17.63-31l.25-42c.08-11.33 8.21-25.18 18-30.8l36.6-20.78c9.85-5.58 25.89-5.54 35.65.22L338 222.25c9.78 5.69 17.7 19.64 17.65 31z"
      fill="url(#icons_nunet-09_svg__b)"
    />
    <circle
      cx={283.59}
      cy={272.47}
      r={50.09}
      fill="url(#icons_nunet-09_svg__c)"
    />
  </svg>
);

export default SvgIconsnunet09;
