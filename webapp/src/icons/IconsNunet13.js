import React from "react";

const SvgIconsnunet13 = props => (
  <svg
    id="icons_nunet-13_svg__Layer_1"
    data-name="Layer 1"
    viewBox="0 0 600 600"
    {...props}
  >
    <defs>
      <linearGradient
        id="icons_nunet-13_svg__linear-gradient"
        x1={309.92}
        y1={70.26}
        x2={314.45}
        y2={532.95}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#1259a3" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-13_svg__linear-gradient-2"
        x1={313.17}
        y1={373.09}
        x2={313.17}
        y2={578.62}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#00a79d" />
        <stop offset={0.53} stopColor="#17a1b9" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-13_svg__linear-gradient-3"
        x1={306.7}
        y1={168.66}
        x2={313.04}
        y2={481.03}
        xlinkHref="#icons_nunet-13_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-13_svg__linear-gradient-4"
        x1={234.09}
        y1={355.33}
        x2={385.08}
        y2={355.33}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#c8e4f1" />
        <stop offset={0.19} stopColor="#c8e4f1" stopOpacity={0.93} />
        <stop offset={0.55} stopColor="#c8e4f1" stopOpacity={0.75} />
        <stop offset={1} stopColor="#c8e4f1" stopOpacity={0.5} />
      </linearGradient>
      <linearGradient
        id="icons_nunet-13_svg__linear-gradient-5"
        x1={253.25}
        y1={361.67}
        x2={360.63}
        y2={361.67}
        xlinkHref="#icons_nunet-13_svg__linear-gradient-4"
      />
      <style>{".icons_nunet-13_svg__cls-2{fill:#fff;opacity:.76}"}</style>
    </defs>
    <rect
      x={130.82}
      y={153.65}
      width={362.31}
      height={253.88}
      rx={9.92}
      fill="url(#icons_nunet-13_svg__linear-gradient)"
    />
    <path
      className="icons_nunet-13_svg__cls-2"
      d="M178.61 169.45h-10.12c-13 0-23.55 11.62-23.55 26v170.47c0 14.33 10.54 25.95 23.55 25.95h287.64c13 0 23.54-11.62 23.54-25.95V195.4c0-14.33-10.54-26-23.54-26z"
    />
    <rect
      x={269.93}
      y={203.17}
      width={79.31}
      height={76.81}
      rx={9.8}
      stroke="#125aa3"
      strokeWidth={10}
      fill="none"
      strokeMiterlimit={10}
    />
    <rect
      x={120}
      y={406.64}
      width={386.33}
      height={30.65}
      rx={7.43}
      fill="url(#icons_nunet-13_svg__linear-gradient-2)"
    />
    <ellipse
      className="icons_nunet-13_svg__cls-2"
      cx={311.98}
      cy={421.97}
      rx={10.01}
      ry={10.37}
    />
    <rect
      x={232.01}
      y={247.91}
      width={155.15}
      height={125.09}
      rx={12}
      fill="url(#icons_nunet-13_svg__linear-gradient-3)"
    />
    <path
      stroke="url(#icons_nunet-13_svg__linear-gradient-4)"
      fill="none"
      strokeMiterlimit={10}
      d="M234.09 355.33h150.99"
    />
    <path
      stroke="url(#icons_nunet-13_svg__linear-gradient-5)"
      fill="none"
      strokeMiterlimit={10}
      d="M253.25 361.67h107.38"
    />
    <path
      className="icons_nunet-13_svg__cls-2"
      d="M330.23 297.49c0-11.05-9.25-20-20.65-20s-20.65 8.95-20.65 20a19.71 19.71 0 007.69 15.51v29.4h26.73v-30a19.65 19.65 0 006.88-14.91z"
    />
  </svg>
);

export default SvgIconsnunet13;
