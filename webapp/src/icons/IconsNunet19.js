import React from "react";

const SvgIconsnunet19 = props => (
  <svg
    id="icons_nunet-19_svg__Layer_1"
    data-name="Layer 1"
    viewBox="0 0 600 600"
    {...props}
  >
    <defs>
      <linearGradient
        id="icons_nunet-19_svg__linear-gradient"
        x1={124.83}
        y1={442.8}
        x2={466.04}
        y2={460.86}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#00a79d" />
        <stop offset={0.53} stopColor="#17a1b9" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet-19_svg__linear-gradient-2"
        x1={128.02}
        y1={364.47}
        x2={469.23}
        y2={382.53}
        xlinkHref="#icons_nunet-19_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-19_svg__linear-gradient-3"
        x1={131.3}
        y1={284}
        x2={472.5}
        y2={302.06}
        xlinkHref="#icons_nunet-19_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet-19_svg__linear-gradient-4"
        x1={134.57}
        y1={203.53}
        x2={475.78}
        y2={221.6}
        xlinkHref="#icons_nunet-19_svg__linear-gradient"
      />
      <style>{".icons_nunet-19_svg__cls-2{fill:#d0eaf6}"}</style>
    </defs>
    <path
      d="M149.27 400.12s.38 68.87 1.09 70.69c9.41 23.92 78.76 42.5 162.94 42.5 85 0 154.94-19 163.21-43.23l.82-70z"
      fill="url(#icons_nunet-19_svg__linear-gradient)"
    />
    <ellipse
      className="icons_nunet-19_svg__cls-2"
      cx={313.31}
      cy={400.12}
      rx={164.03}
      ry={48.06}
    />
    <path
      d="M149.27 321.61s.38 68.88 1.09 70.69c9.41 23.92 78.76 42.49 162.94 42.49 85 0 154.94-18.95 163.21-43.22l.82-70z"
      fill="url(#icons_nunet-19_svg__linear-gradient-2)"
    />
    <ellipse
      className="icons_nunet-19_svg__cls-2"
      cx={313.31}
      cy={321.61}
      rx={164.03}
      ry={48.06}
    />
    <path
      d="M149.27 241s.38 68.88 1.09 70.7c9.41 23.92 78.76 42.49 162.94 42.49 85 0 154.94-18.95 163.21-43.22l.82-70z"
      fill="url(#icons_nunet-19_svg__linear-gradient-3)"
    />
    <ellipse
      className="icons_nunet-19_svg__cls-2"
      cx={313.31}
      cy={240.97}
      rx={164.03}
      ry={48.06}
    />
    <path
      d="M149.27 160.34s.38 68.87 1.09 70.69c9.41 23.92 78.76 42.5 162.94 42.5 85 0 154.94-19 163.21-43.23l.82-70z"
      fill="url(#icons_nunet-19_svg__linear-gradient-4)"
    />
    <ellipse
      className="icons_nunet-19_svg__cls-2"
      cx={313.31}
      cy={160.34}
      rx={164.03}
      ry={48.06}
    />
    <path
      d="M309.84 357.1h-28.1v-42.91a15.51 15.51 0 00-15.64-15.35h-75.5a15.51 15.51 0 00-15.6 15.35v42.91h-28.1a9.62 9.62 0 00-9.69 9.5v125.29a9.62 9.62 0 009.69 9.5h163a9.62 9.62 0 009.68-9.5V366.6a9.62 9.62 0 00-9.74-9.5zm-65.31 74.34v34.66h-31.42v-33.87a22.59 22.59 0 01-9-17.94c0-12.74 10.86-23.07 24.27-23.07s24.26 10.33 24.26 23.07a22.49 22.49 0 01-8.11 17.15zm23.64-74.34h-79.65v-42.91a2.06 2.06 0 012.08-2h75.5a2.06 2.06 0 012.07 2z"
      fill="#f8931f"
      stroke="#d0eaf6"
      strokeMiterlimit={10}
      strokeWidth={5}
    />
  </svg>
);

export default SvgIconsnunet19;
