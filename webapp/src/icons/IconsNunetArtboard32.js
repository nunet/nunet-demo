import React from "react";

const SvgIconsnunetArtboard32 = props => (
  <svg
    id="icons_nunet_Artboard_3-2_svg__Layer_1"
    data-name="Layer 1"
    viewBox="0 0 600 600"
    {...props}
  >
    <defs>
      <linearGradient
        id="icons_nunet_Artboard_3-2_svg__linear-gradient-6"
        x1={149.46}
        y1={100.19}
        x2={341.12}
        y2={445.61}
        xlinkHref="#icons_nunet_Artboard_3-2_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet_Artboard_3-2_svg__linear-gradient-12"
        x1={135.98}
        y1={257.73}
        x2={281.58}
        y2={357.22}
        xlinkHref="#icons_nunet_Artboard_3-2_svg__linear-gradient-7"
      />
      <linearGradient
        id="icons_nunet_Artboard_3-2_svg__linear-gradient-2"
        x1={486.54}
        y1={245.43}
        x2={516.99}
        y2={245.43}
        gradientTransform="matrix(-1 0 0 1 704.13 0)"
        xlinkHref="#icons_nunet_Artboard_3-2_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet_Artboard_3-2_svg__linear-gradient-3"
        x1={412.38}
        y1={243.4}
        x2={445.25}
        y2={243.4}
        xlinkHref="#icons_nunet_Artboard_3-2_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet_Artboard_3-2_svg__linear-gradient-4"
        x1={112.39}
        y1={147.01}
        x2={304.05}
        y2={492.42}
        xlinkHref="#icons_nunet_Artboard_3-2_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet_Artboard_3-2_svg__linear-gradient-5"
        x1={292.51}
        y1={16.87}
        x2={484.17}
        y2={362.29}
        xlinkHref="#icons_nunet_Artboard_3-2_svg__linear-gradient"
      />
      <linearGradient
        id="icons_nunet_Artboard_3-2_svg__linear-gradient"
        x1={311.05}
        y1={346.38}
        x2={316.05}
        y2={346.38}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#00a79d" />
        <stop offset={0.53} stopColor="#17a1b9" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet_Artboard_3-2_svg__linear-gradient-7"
        x1={310.25}
        y1={362.64}
        x2={314.91}
        y2={480.78}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#1259a3" />
        <stop offset={1} stopColor="#2e9ad6" />
      </linearGradient>
      <linearGradient
        id="icons_nunet_Artboard_3-2_svg__linear-gradient-8"
        x1={308.76}
        y1={362.7}
        x2={313.42}
        y2={480.84}
        xlinkHref="#icons_nunet_Artboard_3-2_svg__linear-gradient-7"
      />
      <linearGradient
        id="icons_nunet_Artboard_3-2_svg__linear-gradient-9"
        x1={351.05}
        y1={277.04}
        x2={455.72}
        y2={360.98}
        xlinkHref="#icons_nunet_Artboard_3-2_svg__linear-gradient-7"
      />
      <linearGradient
        id="icons_nunet_Artboard_3-2_svg__linear-gradient-10"
        x1={345.13}
        y1={284.41}
        x2={449.8}
        y2={368.36}
        xlinkHref="#icons_nunet_Artboard_3-2_svg__linear-gradient-7"
      />
      <linearGradient
        id="icons_nunet_Artboard_3-2_svg__linear-gradient-11"
        x1={110.11}
        y1={295.59}
        x2={255.72}
        y2={395.08}
        xlinkHref="#icons_nunet_Artboard_3-2_svg__linear-gradient-7"
      />
      <style>
        {
          ".icons_nunet_Artboard_3-2_svg__cls-4{fill:#00a69c}.icons_nunet_Artboard_3-2_svg__cls-5{fill:#1182b5}.icons_nunet_Artboard_3-2_svg__cls-6{fill:#2e9ad6}.icons_nunet_Artboard_3-2_svg__cls-7{fill:#1259a3}"
        }
      </style>
    </defs>
    <path
      stroke="url(#icons_nunet_Artboard_3-2_svg__linear-gradient)"
      fill="none"
      strokeMiterlimit={10}
      strokeWidth={5}
      d="M313.55 310.98v70.81"
    />
    <path
      stroke="url(#icons_nunet_Artboard_3-2_svg__linear-gradient-2)"
      fill="none"
      strokeMiterlimit={10}
      strokeWidth={5}
      d="M217.6 216.03h-27.96v61.29"
    />
    <path
      stroke="url(#icons_nunet_Artboard_3-2_svg__linear-gradient-3)"
      fill="none"
      strokeMiterlimit={10}
      strokeWidth={5}
      d="M412.38 214h30.37v61.29"
    />
    <path
      className="icons_nunet_Artboard_3-2_svg__cls-4"
      d="M320.89 296.22l-4.51-7.87c-1.16-2-3-2.09-4.14-.13l-4.29 7.6c-1.11 2-.15 3.64 2.12 3.71l8.8.27c2.28.07 3.19-1.54 2.02-3.58z"
    />
    <path
      className="icons_nunet_Artboard_3-2_svg__cls-4"
      d="M340.29 300.18L324 271.68l-9.47-39.81a21.94 21.94 0 01-5.55-.1l-6.43 40.54-14.85 26.25c-4.52 8-.63 14.78 8.63 15.07l35.75 1.09c9.26.28 12.92-6.26 8.21-14.54zm-16.61 5.19l-18.56-.56c-4.81-.16-6.82-3.67-4.48-7.83l9.05-16c2.34-4.15 6.28-4 8.74.26l9.51 16.6c2.46 4.28.54 7.68-4.26 7.53z"
    />
    <path
      className="icons_nunet_Artboard_3-2_svg__cls-5"
      d="M332 174.61a4.89 4.89 0 10-6.83 1.06 4.9 4.9 0 006.83-1.06z"
    />
    <path
      className="icons_nunet_Artboard_3-2_svg__cls-5"
      d="M310 210.06a23.68 23.68 0 015.06 2l11.38-20.19a20.23 20.23 0 10-11.53-4.78zm7.82-45.82a12.67 12.67 0 112.77 17.76 12.68 12.68 0 01-2.74-17.76zM357.5 189a1.83 1.83 0 102.55-.57 1.86 1.86 0 00-2.55.57z"
    />
    <path
      className="icons_nunet_Artboard_3-2_svg__cls-5"
      d="M352.4 185.73a8.15 8.15 0 00-1 6.68l-33.67 18.77c.24.27 2.36 3.25 2.56 3.55l34.25-18 .1.07a8.1 8.1 0 10-2.22-11.1zm10.3 6.69a4.38 4.38 0 11-1.22-6.14 4.43 4.43 0 011.22 6.14z"
    />
    <path
      className="icons_nunet_Artboard_3-2_svg__cls-6"
      d="M273.43 195.73l28.4 20.4a14.3 14.3 0 011.35-1.54l-24.58-25 1.14-5A6.42 6.42 0 00278 179l-5-4.66a6.45 6.45 0 00-5.65-1.3l-6.55 2a6.47 6.47 0 00-3.95 4.25l-1.5 6.71a6.45 6.45 0 001.71 5.55l5 4.66a6.45 6.45 0 005.65 1.3zm-10.32-4.08l-1.6-1.49a6.41 6.41 0 01-1.7-5.55l.48-2.12a6.44 6.44 0 014-4.25l2.08-.64a6.45 6.45 0 015.66 1.29l1.59 1.49a6.41 6.41 0 011.71 5.55l-.48 2.12a6.47 6.47 0 01-4 4.26l-2.08.63a6.45 6.45 0 01-5.66-1.29z"
    />
    <path
      className="icons_nunet_Artboard_3-2_svg__cls-6"
      d="M269.55 188.31a2 2 0 001.18-1.26l.45-2a1.88 1.88 0 00-.5-1.65l-1.5-1.39a1.9 1.9 0 00-1.68-.39l-1.95.6a1.92 1.92 0 00-1.18 1.27l-.46 2a2 2 0 00.51 1.66l1.5 1.38a1.9 1.9 0 001.68.39zM387 199.68a11.83 11.83 0 00-9.24 5.34l-4.25 7.37-56 3.36a16.49 16.49 0 010 2.63l54.2 8.62 6.11 10.54a11.85 11.85 0 009.25 5.33h12.59a11.83 11.83 0 009.24-5.35l6.28-10.9a11.88 11.88 0 000-10.68L408.86 205a11.85 11.85 0 00-9.26-5.34zm17.56 12.75l2 3.48a11.8 11.8 0 010 10.67l-2 3.48a11.82 11.82 0 01-9.24 5.34h-4a11.87 11.87 0 01-9.26-5.34l-2-3.47a11.82 11.82 0 010-10.68l2-3.47a11.84 11.84 0 019.25-5.35h4a11.87 11.87 0 019.27 5.34z"
    />
    <path
      className="icons_nunet_Artboard_3-2_svg__cls-6"
      d="M391.58 215.24a3.29 3.29 0 00-2.58 1.49l-1.75 3a3.27 3.27 0 000 3l1.75 3a3.31 3.31 0 002.58 1.48h3.51a3.29 3.29 0 002.57-1.49l1.75-3a3.33 3.33 0 000-3l-1.76-3a3.29 3.29 0 00-2.58-1.49z"
    />
    <path
      className="icons_nunet_Artboard_3-2_svg__cls-7"
      d="M341.11 241.45a7.8 7.8 0 00-2.7.19l-21.17-15.74a11.52 11.52 0 01-1.44 1.44l17 19.9a5.19 5.19 0 00-.12.79l-.58 6.41a7.69 7.69 0 007.13 8.11l6.79.66a7.54 7.54 0 008.44-6.59l.57-6.41a7.69 7.69 0 00-7.13-8.1zm9.35 9.6l-.34 3.86a4.54 4.54 0 01-5.09 4l-4.09-.4a4.62 4.62 0 01-4.29-4.88l.35-3.86a4.54 4.54 0 015.08-4l4.09.4a4.62 4.62 0 014.29 4.88zM214.64 227.09l7.79 8.68a13.37 13.37 0 0018.88 1L250 229a13.28 13.28 0 002-2.26l46.25-5.5a14.1 14.1 0 010-5.22l-46.79-5.38c-.14-.17-.27-.35-.42-.52l-7.79-8.68a13.38 13.38 0 00-18.89-1l-8.68 7.78a13.38 13.38 0 00-1.04 18.87zM226.93 206a9.3 9.3 0 0113.15.71l5.42 6.05a9.3 9.3 0 01-.72 13.14l-6.05 5.42a9.3 9.3 0 01-13.14-.72l-5.42-6a9.32 9.32 0 01.72-13.15z"
    />
    <rect
      className="icons_nunet_Artboard_3-2_svg__cls-7"
      x={227.31}
      y={213.09}
      width={11.04}
      height={11.04}
      rx={3.84}
      transform="rotate(-41.88 232.835 218.611)"
    />
    <path
      className="icons_nunet_Artboard_3-2_svg__cls-7"
      d="M340.2 251l-.17 2a2.36 2.36 0 002.19 2.49l2.09.21a2.32 2.32 0 002.6-2l.18-2a2.37 2.37 0 00-2.2-2.5l-2.09-.2a2.32 2.32 0 00-2.6 2zM295.91 216.3a14.67 14.67 0 1018.77-8.86 14.68 14.68 0 00-18.77 8.86zm20.32 7.29a6.92 6.92 0 11-4.17-8.85 6.92 6.92 0 014.17 8.85z"
    />
    <path
      d="M229.41 296.72v-19.31A6.43 6.43 0 00223 271h-66.7a6.43 6.43 0 00-6.43 6.43v19.31h12.4v-12.89h56v12.87z"
      fill="url(#icons_nunet_Artboard_3-2_svg__linear-gradient-4)"
    />
    <path
      d="M402.67 297.59v-19.31a6.43 6.43 0 016.44-6.43h66.68a6.43 6.43 0 016.43 6.43v19.31h-12.4v-12.87h-56v12.87z"
      fill="url(#icons_nunet_Artboard_3-2_svg__linear-gradient-5)"
    />
    <path
      d="M274 406.82v-19.31a6.43 6.43 0 016.43-6.43h66.68a6.43 6.43 0 016.43 6.43v19.31h-12.4V394h-56v12.87z"
      fill="url(#icons_nunet_Artboard_3-2_svg__linear-gradient-6)"
    />
    <path
      d="M375.41 406.82H252.76a14.48 14.48 0 00-14.43 14.44v77.12a14.47 14.47 0 0014.43 14.43h122.65a14.48 14.48 0 0014.44-14.43v-77.12a14.49 14.49 0 00-14.44-14.44zm-61.65 91.38a38.39 38.39 0 1138.38-38.38 38.38 38.38 0 01-38.38 38.38z"
      fill="url(#icons_nunet_Artboard_3-2_svg__linear-gradient-7)"
    />
    <path
      d="M334.14 435.11l-4.23-1.69-2 5a27.09 27.09 0 00-5.19-2.77 28 28 0 00-3.65-1.16l1.86-4.65-4.22-1.7-2.26 5.63a27.06 27.06 0 00-21.08 46l-2.28 5.68 4.23 1.7 1.71-4.27a27.62 27.62 0 005.54 3 26.06 26.06 0 003.24 1.06l-1.52 3.79 4.23 1.69 1.88-4.69a27.07 27.07 0 0015.83-3.57l-3.59-8.33A17.74 17.74 0 01306 477.4a17.93 17.93 0 1113.35-33.29c6.64 2.66 9.86 9 10.27 15.67l10-1.27a27.14 27.14 0 00-8-17.08z"
      fill="url(#icons_nunet_Artboard_3-2_svg__linear-gradient-8)"
    />
    <path
      d="M504.08 297.59H381.43A14.48 14.48 0 00367 312v77.12a14.49 14.49 0 0014.44 14.44h122.64a14.49 14.49 0 0014.44-14.44V312a14.48 14.48 0 00-14.44-14.41zm-105.52 52.34a22.66 22.66 0 1122.66-22.66 22.65 22.65 0 01-22.66 22.66z"
      fill="url(#icons_nunet_Artboard_3-2_svg__linear-gradient-9)"
    />
    <path
      d="M410.19 313.69l-2.28-.91-1.08 2.69A14.78 14.78 0 00404 314a14.52 14.52 0 00-2-.63l1-2.51-2.28-.91-1.22 3a14.62 14.62 0 00-11.38 24.84l-1.12 3.06 2.28.91.93-2.3a14.59 14.59 0 003 1.63 16.29 16.29 0 001.75.58l-.82 2 2.28.91 1-2.53a14.63 14.63 0 008.55-1.93l-1.97-4.45a9.56 9.56 0 01-9 .84 9.68 9.68 0 117.21-18c3.58 1.43 5.32 4.84 5.54 8.46l5.42-.69a14.74 14.74 0 00-4.33-9.22z"
      fill="url(#icons_nunet_Artboard_3-2_svg__linear-gradient-10)"
    />
    <path
      d="M251 296.94H128.32a14.48 14.48 0 00-14.44 14.43v77.12a14.49 14.49 0 0014.44 14.44H251a14.48 14.48 0 0014.43-14.44v-77.12A14.47 14.47 0 00251 296.94zm-14.18 52.34a22.66 22.66 0 1122.66-22.66 22.65 22.65 0 01-22.69 22.66z"
      fill="url(#icons_nunet_Artboard_3-2_svg__linear-gradient-11)"
    />
    <path
      d="M248.42 313l-2.28-.91-1.08 2.69a14.78 14.78 0 00-2.81-1.5 14.09 14.09 0 00-2-.63l1-2.51-2.28-.91-1.22 3a14.62 14.62 0 00-11.38 24.84l-1.23 3.06 2.28.91.93-2.3a14.59 14.59 0 003 1.63 16.29 16.29 0 001.75.58l-.82 2 2.28.91 1-2.53a14.62 14.62 0 008.54-1.93l-1.89-4.4a9.56 9.56 0 01-9 .84 9.68 9.68 0 117.21-18c3.58 1.43 5.32 4.84 5.54 8.46l5.42-.69a14.69 14.69 0 00-4.33-9.22z"
      fill="url(#icons_nunet_Artboard_3-2_svg__linear-gradient-12)"
    />
  </svg>
);

export default SvgIconsnunetArtboard32;
