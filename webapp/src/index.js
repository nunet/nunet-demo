import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";

import * as serviceWorker from "./serviceWorker";

// import style down here is intentional i.e. don't change it
const createBrowserHistory = require("history").createBrowserHistory;

// use history package to set up react-router history
const browserHistory = createBrowserHistory();


// subscribe to history changes to handle google analytics
browserHistory.listen((location, action) => {
    //! assumes window.gtag is setup in the main header html(I hope we wouldn't forget setting it up right)
    if (typeof window.gtag === "function") {
        let page_path = location.pathname;

        window.gtag("config", "UA-155054429-1", {
            page_path: page_path
        });
        console.log(
            `gtag: from location.pathname: ${location.pathname} sent page_path: ${page_path}`
        );
    } else {
        console.warn(
            `[Nunet Webapp] Couldnot find global gtag function. 
            How the hell am I going to send data to google analytics!! 
            \nFor location: ${location} and action: ${action}`
        );
    }
});

ReactDOM.render(
    <App history={browserHistory} />,
    document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
